//
//  LoadingHelper.swift
//  DMS
//
//  Created by KID on 9/15/17.
//  Copyright © 2017 KD. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

let LOADING = LoadingHelper.shareIntanse

class LoadingHelper {
    
    static let shareIntanse = LoadingHelper()
    
    func show() {
        
        SVProgressHUD.setDefaultMaskType(.gradient)
        SVProgressHUD.show()
    }
    
    func dismiss() {
        SVProgressHUD.dismiss()
    }
    
}

