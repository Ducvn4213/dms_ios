//
//  Config.swift
//  DMS
//
//  Created by KID on 10/10/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

let min_pass_count = 4

let vietnam = "vi"
let english = "en"

//let base_url = "http://idn.com.vn/BSGKG_WebSrv/IDNWebSerives.asmx/"

let sync_checkin_type = "sync_checkin_type"
let sync_order_type = "sync_order_type"

let get_note_url = "/id_CheckIn_Notes_LoadbyCustCode"
let get_note_type_url = "/iD_NotesGroup_Load"
let add_note_url = "/iD_CheckIn_Notes_Add"
let get_channels_url = "/IDN_CHANNEL_Load"
let get_customer_group_url = "/IDN_CUST_GRP_Load"
let get_orders_url = "/IDN_DMS_OrderList_Search"
let get_type_visit_url = "/IDN_Visit_List"
let get_type_order_url = "/IDN_OrderType_List"
let get_status_order_url = "/IDN_DMS_StatusOrder_List"
let get_warehouse_url = "/IDN_DMS_WhsList"
let get_route_url = "/IDN_ROUTE_Load"
let stock_add_url = "/iD_CheckIn_InStock_Add"
let checkin_url = "/iD_CheckInLog_Add"
let checkout_url = "/iD_CheckOutLog_Add"
let add_customer_url = "/iD_OCRD_Add"
let search_customer_url = "/IDN_DMS_OCRD_Search"
let customer_add_order_url = "/iD_CORDR_CustomerOrder_Add"
let get_item_price_url = "/iD_ItemPrice_byCustCode_UoM"
let get_promotion_product_url = "/iD_ItemPromotionList_byCust"
let item_list_sales_search_url = "/IDN_ItemListSales_Search"
let item_list_checkin_instock_url = "/IDN_ItemList_CheckIn_InStock"
let get_statistic_url = "/id_CheckIn_Statistics_LoadbyCustCode"
let get_item_by_barcode = "/IDN_DMS_ItemInfo_ByBarCode"
let get_order_by_docentry = "/IDN_DMS_Order_ByDocEntry"
let order_updatedelivery_cancled = "/IDN_DMS_Order_UpdateDelivery_Cancled"
let order_updatedelivery_byitem = "/IDN_DMS_Order_UpdateDelivery_ByItem"
let order_updatedelivery_update = "/IDN_DMS_Order_UpdateDelivery_All"
let get_leave_reason = "/iD_LeaveReason_Load"
let get_leave_type = "/iD_LeaveType_Load"
let get_leave_log = "/iD_Leave_Log"
let add_leave_ticket = "/iD_Leave_Add"
let approve_leave = "/iD_Leave_Approve"




