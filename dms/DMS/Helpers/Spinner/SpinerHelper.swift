//
//  SARMaterialDesignSpinner.swift
//  SARMaterialDesignSpinner
//
//  Created by Saravanan on 16/02/16.
//  Copyright © 2016 Saravanan. All rights reserved.
//
import Foundation
import UIKit

class SpinnerHelper {
    
    static let sharedInstance = SpinnerHelper()
    private var spinnersInViews = [UIView : SARMaterialDesignSpinner]()
    
    init() {
        
    }
    
    private func instantiateSpinner() -> SARMaterialDesignSpinner {
        
        let spinner = SARMaterialDesignSpinner(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        spinner.lineWidth = 1.5
        spinner.tintColor = UIColor.white
        return spinner
    }
    
    func startAnimatingInView(containingView: UIView?, withColor color: UIColor = UIColor.white) {
        
        if let view = containingView {
            
            if let _ = self.spinnersInViews[view] {
                // ignore this request. Probably tableView delegate called twice- spinner is already here
            } else {
                
                let spinner = instantiateSpinner()
                spinner.tintColor = color
                spinner.center = view.center
                spinner.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin]
                spinner.translatesAutoresizingMaskIntoConstraints = true
                view.addSubview(spinner)
                view.layoutSubviews()
                self.spinnersInViews[view] = spinner
                spinner.startAnimating()
            }
        }
    }
    
    func startAnimatingInBounds(containingView: UIView?, withColor color: UIColor = UIColor.white) {
        
        if let view = containingView {
            
            if let _ = self.spinnersInViews[view] {
                // ignore this request. Probably tableView delegate called twice- spinner is already here
            } else {
                
                let spinner = instantiateSpinner()
                spinner.tintColor = color
                spinner.center = view.center
                spinner.autoresizingMask = [.flexibleTopMargin, .flexibleRightMargin, .flexibleLeftMargin, .flexibleBottomMargin]
                spinner.translatesAutoresizingMaskIntoConstraints = true
                let boundView = UIView(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
                boundView.tag = 1111
                boundView.addSubview(spinner)
                
                view.addSubview(boundView)
                view.layoutSubviews()
                self.spinnersInViews[view] = spinner
                spinner.startAnimating()
            }
        }
    }
    
    func stopAnimatingInView(containingView: UIView?) {
        if let view = containingView {
            
            if let spinner = self.spinnersInViews[view] {
                
                spinner.stopAnimating()
                spinner.removeFromSuperview()
                if let boundView = view.viewWithTag(1111) {
                    boundView.removeFromSuperview()
                }
                self.spinnersInViews[view] = nil
            } else {
                
                print("No such spinner")
            }
        }
    }
    
    func stopAnimatingInAllViews() {
        
        self.spinnersInViews.forEach { (dict: (containingView: UIView, spinner: SARMaterialDesignSpinner)) in
            
            dict.spinner.stopAnimating()
            dict.spinner.removeFromSuperview()
            self.spinnersInViews[dict.containingView] = nil
        }
    }
}
