//
//  UIBarButtonItem+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 3/6/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    func show(withTintColor tintColor: UIColor? = nil) {
        self.isEnabled = true
        self.tintColor = tintColor ?? UIColor.white
    }
    
    func hide() {
        self.isEnabled = false
        self.tintColor = UIColor.clear
    }
}
