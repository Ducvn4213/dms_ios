//
//  UIView+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 2/18/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit

extension UIView {
    
    func bindFrameToSuperviewBounds() {
        guard let superview = self.superview else {
            print("Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
            return
        }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[subview]-0-|",
                                                                options: NSLayoutFormatOptions(), metrics: nil, views: ["subview": self]))
        superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-0-[subview]-0-|",
                                                                options: NSLayoutFormatOptions(), metrics: nil, views: ["subview": self]))
    }
    
    func makeCircle() {
        layer.cornerRadius = bounds.height / 2
        layer.masksToBounds = true
    }
    
    func dropShadow(_ opacity: CFloat = 0.07, radius: CGFloat = 1) {
        layer.shadowOpacity = opacity
        layer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        layer.shadowRadius = radius
        layer.masksToBounds = false
        let shadowPath: CGPath = UIBezierPath(roundedRect: self.layer.bounds, cornerRadius: 3).cgPath
        layer.shadowPath = shadowPath
        layer.shouldRasterize = true
        layer.rasterizationScale = UIScreen.main.scale
    }
    
    func makeDefaultCornerRadius(_ radius: CGFloat = 3) {
        layer.cornerRadius = radius
        clipsToBounds = true
    }
    
    func removeShadow() {
        layer.shadowPath = nil
    }
    
    func removeAllSubviews() {
        for subUIView in self.subviews as [UIView] {
            subUIView.removeFromSuperview()
        }
    }
}

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
    @IBInspectable var border_Width: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var border_Color: UIColor? {
        get {
            guard let color = layer.borderColor else {
                return .clear
            }
            return UIColor(cgColor: color)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    class func fromNib() -> UIView {
        return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)!.first as! UIView
    }
}
