//
//  UITextField+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 3/13/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit

extension UITextField {
    func addLeftView(with image:UIImage) {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        let imageView = UIImageView(frame: CGRect(x: 7, y: 5, width: 17, height: 17))
        imageView.image = image
        
        view.addSubview(imageView)
        self.leftViewMode = .always
        self.leftView = view
    }
}
