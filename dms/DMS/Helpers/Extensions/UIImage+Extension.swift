//
//  UIImage+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 3/4/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit

extension UIImageView {
    func createCoverBlurImage(with image : UIImage?) {
        self.image = image
    
        // create effect
        let blur = UIBlurEffect(style: UIBlurEffectStyle.light)
    
        // add effect to an effect view
        let effectView = UIVisualEffectView(effect: blur)
        effectView.frame = self.frame
    
        self.addSubview(effectView)
    }
}

extension UIImage {
    func resizeToWidth(width:CGFloat, compressionQuality quality:CGFloat=0.5)-> UIImage {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        
        
        imageView.contentMode = UIViewContentMode.scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContext(imageView.bounds.size)
        imageView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let img = UIGraphicsGetImageFromCurrentImageContext()
        let imageData = UIImageJPEGRepresentation(img!, quality)!
        UIGraphicsEndImageContext()
        return UIImage(data: imageData)!
    }
    
    class func image(fromLayer layer: CALayer) -> UIImage {
        UIGraphicsBeginImageContext(layer.frame.size)
        layer.render(in: UIGraphicsGetCurrentContext()!)
        let outputImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return outputImage!
    }
    
    class func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: size.width, height:size.height)
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        color.setFill()
        UIRectFill(rect)
        let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
}
