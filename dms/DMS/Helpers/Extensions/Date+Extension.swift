//
//  Date+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 3/18/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import Foundation

extension Date {
    init(string: String, format: String) {
        let formater = DateFormatter()
        formater.dateFormat = format
        formater.locale = Locale.init(identifier: "en_GB")
        
        self = formater.date(from: string) ?? Date()
    }
    
    func getElapsedInterval() -> String {
        let string = getElapsedIntervalImp()
        if string == "" {
            return "a moment ago"
        } else {
            return string + " " + "ago"
        }
        
    }
    
    private func getElapsedIntervalImp() -> String {
        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year ?? 0
        
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "year" :
                "\(interval)" + " " + "years"
        }
        
        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "month" :
                "\(interval)" + " " + "months"
        }
        
        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "day" :
                "\(interval)" + " " + "days"
        }
        
        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "hour" :
                "\(interval)" + " " + "hours"
        }
        
        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " " + "minute" :
                "\(interval)" + " " + "minutes"
        }
        
        return ""
    }
    
    func toStringHHmmddMMyyy() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        dateFormatter.dateFormat = "hh:mm dd-MM-yyyy"
        let result = dateFormatter.string(from: self)
        return result
    }
    
    func toStringDDMMYYYY() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let result = dateFormatter.string(from: self)
        return result
    }
    
    func toStringYYYYMMDD() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let result = dateFormatter.string(from: self)
        return result
    }
    
    func toStringMMDDYYYY() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale.init(identifier: "en_GB")
        dateFormatter.dateFormat = "MM/dd/yyyy"
        let result = dateFormatter.string(from: self)
        return result
    }
}
