//
//  String+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 4/4/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit

extension String {
    func heightForView(font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = self
        
        label.sizeToFit()
        return label.frame.height
    }
    func stringToDateTimeString(inputFormat: String, outputFormat : String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = inputFormat
        
        if let date = dateFormatter.date(from: self) {
            dateFormatter.dateFormat = outputFormat
            let dateString = dateFormatter.string(from:date)
            return dateString
        } else {
            return ""
        }
    }
}
