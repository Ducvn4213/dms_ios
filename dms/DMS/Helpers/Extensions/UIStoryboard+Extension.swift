//
//  UIStoryboard+Extension.swift
//  DMS
//
//  Created by KID on 11/10/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import UIKit

extension UIStoryboard {
    
    
    class func storyboardWithName(name:String) -> UIStoryboard {
        
        return UIStoryboard(name: name, bundle:  Bundle.main)
    }
    
    //MARK: Authenticate
    
    class func checkinMapViewController() -> CheckinMapViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "CheckinMapViewController") as! CheckinMapViewController
    }
    
    
    class func productSearchViewController() -> ProductSearchViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "productsearch") as! ProductSearchViewController
    }
    
    class func addCustomerViewController() -> AddCustomerViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "addcustomer") as! AddCustomerViewController
    }
    
    class func newOrderViewController() -> NewOrderViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "NewOrderViewController") as! NewOrderViewController
    }
    
    class func orderProductViewController() -> AddProductViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "orderproductsearch") as! AddProductViewController
    }
    
    class func statisticViewController() -> StatisticViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "stastic") as! StatisticViewController
    }
    
    class func orderDetailViewController() -> OrderDetailViewController {
        return self.storyboardWithName(name: "Main").instantiateViewController(withIdentifier: "OrderDetailViewController") as! OrderDetailViewController
    }
    
    
}

