//
//  UIViewController+Extension.swift
//  Athina
//
//  Created by Thanh Tanh on 3/14/17.
//  Copyright © 2017 Athina. All rights reserved.
//

import UIKit
import PopupDialog

extension UIViewController {
    func showAlert(withTitle title:String, message: String) {
        let popup = PopupDialog(title: title, message: message)
        let okButton = CancelButton(title: "OK", action: nil)
        popup.addButton(okButton)
        self.present(popup, animated: true, completion: nil)
    }
    
    // show toast message like android
    func showToast(message : String) {
        
        let toastLabel = UILabel(frame: CGRect(x: 10, y: self.view.frame.size.height - 100, width: self.view.frame.size.width - 20, height: 45))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIApplication.topViewController()?.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 1.0, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }

    
    func gotoViewController(name : String, board : String) {
        let storyboard: UIStoryboard = UIStoryboard(name: board, bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: name) as UIViewController
        self.present(vc, animated: false, completion: nil)
    }
    
    func gotoViewControllerInNavigationWithIndentifier(name : String, board: String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: board, bundle:nil)
        let homeViewController = storyBoard.instantiateViewController(withIdentifier: name)
        
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
    
    func gotoViewController(name : String) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: UIViewController = storyboard.instantiateViewController(withIdentifier: name) as UIViewController
        self.present(vc, animated: false, completion: nil)
    }
    
    func gotoViewControllerInNavigationWithIndentifier(name : String) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let homeViewController = storyBoard.instantiateViewController(withIdentifier: name)
        
        self.navigationController?.pushViewController(homeViewController, animated: true)
    }
    
    func showDialog(title : String, message : String, handlerOK: ((UIAlertAction) -> Swift.Void)? = nil, handlerCancel: ((UIAlertAction) -> Swift.Void)? = nil) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        if (handlerOK != nil) {
            let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: handlerOK)
            alertController.addAction(ok)
        } else {
            let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
            alertController.addAction(ok)
        }
        
        if (handlerCancel != nil) {
            let cancel = UIAlertAction(title: "Cancel".localized(), style: .default, handler: handlerCancel)
            alertController.addAction(cancel)
        }
        
        present(alertController, animated: true, completion: nil)
    }
}

extension UIApplication {
    
    // get current top view controller
    class func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
