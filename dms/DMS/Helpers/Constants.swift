//
//  Constants.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

let notImplementMessage = "Chưa kết nối được với SAP. Vui lòng xem lại"

let autologinkey = "iDMS_autologin"

let sale_id_key = "sale_id_key"

let working_date = "working_date"
