//
//  ScreenSize.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

public enum ScreenType {
    case ip4S
    case ip5
    case ip6
    case ip6Plus
}

public struct ScreenSize {
    public static let IP4S = CGSize(width: 320, height: 480)
    public static let IP5 = CGSize(width: 320, height: 568)
    public static let IP6 = CGSize(width: 375, height: 667)
    public static let IP6Plus = CGSize(width: 414, height: 736)
    public static let Current = UIScreen.main.bounds.size
    
    public static let CurrentType : ScreenType = {
        if Current == IP5 {
            return .ip5
        }
        
        if Current == IP6 {
            return .ip6
        }
        
        if Current == IP6Plus {
            return .ip6Plus
        }
        
        return .ip4S
    }()
    
    public static var Layout = ScreenSize.IP6 {
        didSet {
            LayoutRatio = Current.height / ScreenSize.Layout.height
            
            let currentRatio = ScreenSize.Current.height / ScreenSize.Current.width
            let layoutRatio = ScreenSize.Layout.height / ScreenSize.Layout.width
            Ratio = currentRatio / layoutRatio
        }
    }
    
    
    fileprivate static var LayoutRatio = ScreenSize.Current.height / ScreenSize.Layout.height
    fileprivate static var Ratio: CGFloat = {
        let currentRatio = ScreenSize.Current.height / ScreenSize.Current.width
        let layoutRatio = ScreenSize.Layout.height / ScreenSize.Layout.width
        
        return currentRatio / layoutRatio
    }()
}

/**
 Standardize layout config depend on screen resolution.
 - Parameter num: layout config.
 - Returns: Config after standardize.
 */
public func ST(_ num: AnyObject, powValue: CGFloat = 1) -> CGFloat {
    return ScreenSize.LayoutRatio * (num as? CGFloat ?? 0) * pow(ScreenSize.Ratio, powValue)
}

