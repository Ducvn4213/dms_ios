//
//  GlobalFunc.swift
//  LocationSearch
//
//  Created by KID on 9/15/17.
//  Copyright © 2017 hoan vu. All rights reserved.
//

import UIKit
import RealmSwift

let APPDELEGATE = UIApplication.shared.delegate as! AppDelegate
let REALM = try! Realm()

func savePref(data : Any, key : String) {
    UserDefaults.standard.set(data, forKey: key)
}

func getPref(key : String) -> Any? {
    return UserDefaults.standard.object(forKey: key)
}

func removePref(key : String) {
    UserDefaults.standard.removeObject(forKey: key)
}

func getCurrentApiUrl() -> String {
    let data = getPref(key: "API_URL")
    if (data == nil) {
        return "http://idn.com.vn/BSGKG_WebSrv/IDNWebSerives.asmx"
    }
    
    return data as! String
}

func setNewApiUrl(data: String) {
    savePref(data: data, key: "API_URL")
}

func getDetailLanguage(language : String) -> String {
    switch language {
    case vietnam:
        return "Vietnamese".localized()
    default:
        return "English".localized()
    }
}

func ISOTimeToNomalTime(input: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    var date = dateFormatter.date(from: input)
    
    if (date == nil) {
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        
        date = dateFormatter.date(from: input)
        
        if (date == nil) {
            return input
        }
    }
    
    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
    return dateFormatter.string(from: date!)
}

func getCurrentISOTime() -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    
    let date = Date()
    
    return dateFormatter.string(from: date)
}

func getCurrentNormalTime() -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy"
    
    let date = Date()
    
    return dateFormatter.string(from: date)
}

func normalTimeToISOTime(input: String) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd/MM/yyyy HH:mm"
    
    let date = dateFormatter.date(from: input)
    
    if (date == nil) {
        return input
    }
    
    dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    return dateFormatter.string(from: date!)
}

func secondsToTime(input: Int64) -> String {
    let h = input / 3600
    let m = input / 60
    let s = input % 60
    
    return correctTimeUnit(input: h) + ":" + correctTimeUnit(input: m) + ":" + correctTimeUnit(input: s)
}

func correctTimeUnit(input: Int64) -> String {
    if (input < 10) {
        return "0" + String(input)
    }
    
    return String(input)
}

func loginDialog(vc : UIViewController) {
    let alertController = UIAlertController(title: "Login request".localized(), message: "Please login to use this feature".localized(), preferredStyle: .alert)
    
    let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
        vc.gotoViewControllerInNavigationWithIndentifier(name: "login")
    })
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
        
    })
    alertController.addAction(ok)
    alertController.addAction(cancel)
    
    vc.present(alertController, animated: true, completion: nil)
}
