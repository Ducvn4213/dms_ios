//
//  CustomerDetailViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import MXPagerView
import HMSegmentedControl
import GoogleMaps
import PopupDialog
import RealmSwift
import QRCodeReader
import AVFoundation

class CustomerDetailViewController: BaseViewController {
    
    @IBOutlet weak var countdownLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactPersonLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    
    // @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var noteButton: UIButton!
    @IBOutlet weak var checkoutButton: UIButton!
    
    @IBOutlet weak var bookButton: UIButton!
    
    
    @IBOutlet weak var pager:MXPagerView!
    @IBOutlet weak var segmentedControl:HMSegmentedControl!
    
//    @IBOutlet weak var heightForPager: NSLayoutConstraint!
    
    var captureView:CustomerDetailCaptureView?
    var inventoryViewController: InventoryViewController!
    var bookingViewController: InventoryViewController!
    
    var customer: Customer?
    
    var timer: Timer?
    var seconds = 0
    
    var checkinId: String!
    
    var notes = List<Note>()
    
    var isSave = false
    
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader = QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode], captureDevicePosition: .back)
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        
        pager.delegate = self
        pager.dataSource = self
        pager.reloadData()
        
        displayCustomer()
//        fetchProducts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.runCounter()
    }
    
    func setupUI() {
        if let storyboard = self.storyboard {
            inventoryViewController = storyboard.instantiateViewController(withIdentifier: "InventoryViewController") as! InventoryViewController
            bookingViewController = storyboard.instantiateViewController(withIdentifier: "InventoryViewController") as! InventoryViewController
            inventoryViewController.delegate = self
            bookingViewController.delegate = self
            inventoryViewController.didMove(toParentViewController: self)
            bookingViewController.didMove(toParentViewController: self)
        }
        
        self.setupSegmentedControl()
        
        self.noteButton.alignImageAndTitleVertically(padding: 6)
        self.checkoutButton.alignImageAndTitleVertically(padding: 6)
        
        self.bookButton.alignImageAndTitleVertically(padding: 6)
    }
    
    func setupSegmentedControl() {
        segmentedControl.selectionIndicatorLocation = .down
        segmentedControl.selectionIndicatorColor = UIColor(hexString: "DC4B32")
        segmentedControl.selectionIndicatorHeight = 3
        segmentedControl.selectionStyle = .fullWidthStripe
        segmentedControl.selectedTitleTextAttributes = [
            NSForegroundColorAttributeName: UIColor.black
        ]
        
        segmentedControl.sectionTitles = ["Tồn kho", "Hình ảnh", "Vị trí"]
        
        segmentedControl.titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 13),
                                                NSForegroundColorAttributeName: UIColor.darkText]
        segmentedControl.addTarget(self, action: #selector(CustomerDetailViewController.segmentDidSelect), for: .valueChanged)
    }
    
    func fetchProducts() {
        SpinnerHelper.sharedInstance.startAnimatingInView(containingView: self.view)
        ProductServices.getCustomerProducts(cusCode: (customer?.CardCode)!, onSucess: { (products) in
            SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
            APPDELEGATE.originStocks = products
//            APPDELEGATE.stocks = products
            //            self.bookingViewController.products = products
            UIView.animate(withDuration: 0, animations: {
                self.inventoryViewController.tableView.reloadData()
                //                self.bookingViewController.tableView.reloadData()
            }, completion: { (_) in
//                let height = self.inventoryViewController.tableView.contentSize.height
//                self.heightForPager.constant = height + 60
//                self.view.layoutIfNeeded()
            })
            
        }) { (error) in
            SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
            self.showMessage(message: error)
        }
    }
    
    func displayCustomer() {
        guard let customer = self.customer else { return }
        
        self.nameLabel.text = customer.CardName
        self.contactPersonLabel.text = "Người liên hệ: " + customer.ContactPerson!
        self.addressLabel.text = customer.Address
        self.telLabel.text = customer.Tel
    }
    
    func segmentDidSelect(segment: HMSegmentedControl) {
        UIView.animate(withDuration: 0.3) {[weak self]() -> Void in
            guard let `self` = self else { return }
            self.pager.showPage(at: segment.selectedSegmentIndex, animated: true)
        }
    }
    
    func runCounter() {
        if self.timer != nil {
            return
        }
        
        self.timer = Timer.scheduledTimer(timeInterval: 1,
                                          target: self,
                                          selector: #selector(self.updateTime),
                                          userInfo: nil,
                                          repeats: true)
    }
    
    func updateTime() {
        seconds += 1
        
        func intToStringTime(time: Int) -> String {
            var s = "\(time)"
            if s.count == 1 {
                s = "0" + s
            }
            return s
        }
        
        func secondsToHoursMinutesSeconds (seconds : Int) -> (Int, Int, Int) {
            return (seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
        }
        
        let time = secondsToHoursMinutesSeconds(seconds: seconds)
        
        self.countdownLabel.text = String.init(format: "%@:%@:%@", intToStringTime(time: time.0), intToStringTime(time: time.1), intToStringTime(time: time.2))
    }
    
    func savePhoto() {
        self.captureView?.savePhoto()
    }
    
    @IBAction func checkout() {
        
        showDialog(title: "iDMS Management".localized(), message: "Bạn có muốn checkout ngay bây giờ?".localized(), handlerOK: {(UIAlertAction) in
            LOADING.show()
            self.savePhoto()
            
            let syncObj = SyncObject()
            
            syncObj.SyncType = sync_checkin_type
            
            let checkin = Checkin()
            
            let deviceID = UIDevice.current.identifierForVendor!.uuidString
            
            checkin.UserCode = APPDELEGATE.userCode
            checkin.CustCode = self.customer?.CardCode
            checkin.DeviceID = deviceID
            checkin.ListNote = self.notes
            checkin.ListProduct = self.getCheckinProductList()
            syncObj.Checkin = checkin
            
            try! REALM.write {
                REALM.add(syncObj)
            }
            
            SyncService.sharedInstance.processSyncWith(syncObjects: [syncObj], onSucess: {(string) in
                LOADING.dismiss()
                self.showDialog(title: "Checkout", message: "Checkout success!", handlerOK: {(UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                })
                
            }, onError: {(string) in
                LOADING.dismiss()
                self.showToast(message: string)
                self.showDialog(title: "Error", message: "Có lỗi, checkout đã được thêm offline".localized(), handlerOK: {(UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
//            CustomerServices.checkin(UserCode: APPDELEGATE.userCode, CustCode: (self.customer?.CardCode)!, onSucess: {(checkinId) in
//                self.checkinId = checkinId
//                self.checkinNote()
//            }, onError: {(error) in
//                self.showToast(message: error)
//            })
        }, handlerCancel: {(UIAlertAction) in
            
        })
    }
    
    func checkinNote() {
        for note in notes {
            CustomerServices.addNote(UserCode: APPDELEGATE.userCode, CustCode: (customer?.CardCode)!, CheckInID: checkinId, NotesGroup: note.NotesGroup!, NotesRemark: note.NotesRemark!, onSucess: {(noteId) in
                print(noteId)
                if (self.isSave) {
                    self.checkinAddProduct()
                }
            }, onError: {(error) in
                self.showToast(message: error)
                print(error)
            })
        }
        LOADING.dismiss()
        showDialog(title: "Checkout", message: "Checkout success!", handlerOK: {(UIAlertAction) in
            self.navigationController?.popViewController(animated: true)
        })
    }
    
    func checkinAddProduct() {
        for (index, stock) in APPDELEGATE.stocks.enumerated() {
            if index >= APPDELEGATE.originStocks.count || (index < APPDELEGATE.originStocks.count && !APPDELEGATE.originStocks[index].equals(product: stock)) {
                CustomerServices.checkinInStockAdd(UserCode: APPDELEGATE.userCode, CustCode: (customer?.CardCode)!, CheckInID: checkinId, ItemCode: stock.ItemCode!, BarCode: stock.CodeBars!, CaseInStock: stock.even, BottleInStock: stock.retail, Remarks: "", onSucess: {(id) in
                    print(id)
                }, onError: {(error) in
                    print(error)
                })
            }
        }
    }
    
    func getCheckinProductList() -> List<Product> {
        
        let results = List<Product>()
        
        for (index, stock) in APPDELEGATE.stocks.enumerated() {
//            if index >= APPDELEGATE.originStocks.count || (index < APPDELEGATE.originStocks.count && !APPDELEGATE.originStocks[index].equals(product: stock)) {
                results.append(stock)
//            }
        }
        return results
    }
    
    @IBAction func book() {
        let vc = UIStoryboard.newOrderViewController()
        vc.customer = customer
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func note() {
        
        let vs = AllNoteViewController(nibName: "AllNoteViewController", bundle: nil)
        
        // Create the dialog
        let p = PopupDialog(viewController: vs, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Pass a reference to the dialog
        vs.dialog = p
        
        vs.save = { (note) in
            self.notes.append(note)
        }
        
        self.present(p, animated: true, completion: nil)
    }
}

extension CustomerDetailViewController: MXPagerViewDelegate, MXPagerViewDataSource {
    func numberOfPages(in pagerView: MXPagerView) -> Int {
        return 3
    }
    
    func pagerView(_ pagerView: MXPagerView, viewForPageAt index: Int) -> UIView? {
        
        if index == 0 {
            return self.inventoryViewController.view
        } else if index == 3 {
            return self.bookingViewController.view
        } else if index == 1 {
            self.captureView = CustomerDetailCaptureView()
            captureView?.controller = self
            return captureView ?? UIView()
        } else if index == 2 {
            //            let mapView = GMSMapView()
            let mapView = GMSMapView()
            let location2D = CLLocationCoordinate2D(latitude: Double(customer!.LatitudeValue!)!, longitude: Double(customer!.LongitudeValue!)!)
            
            let marker = GMSMarker(position: location2D)
            
            marker.title = customer?.CardName
            marker.map = mapView
            
            mapView.camera = GMSCameraPosition.camera(withTarget: location2D, zoom: 13)
            return mapView
        }
        
        return UIView()
    }
    
    func pagerView(_ pagerView: MXPagerView, didMoveToPageAt index: Int) {
        self.segmentedControl.selectedSegmentIndex = index
        
        var height:CGFloat = 300
        switch index {
        case 0:
            height = self.inventoryViewController.tableView.contentSize.height +  60
            //        case 1:
        //            height = self.bookingViewController.tableView.contentSize.height + 60
        default:
            break
        }
//        self.heightForPager.constant = height
//        self.view.layoutIfNeeded()
    }
}

extension CustomerDetailViewController: InventoryDelegate {
    func didBarcode() {
        readerVC.delegate = self
        readerVC.modalPresentationStyle = .formSheet
        present(readerVC, animated: true, completion: nil)
    }
    
    func didAddedRow() {
//        self.heightForPager.constant += 50
//        self.view.layoutIfNeeded()
    }
    
    func didSave() {
        let alertController = UIAlertController(title: "Save".localized(), message: "Do you want to save?".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: {(UIAlertAction) in
            self.isSave = true
        })
        let cancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel, handler: nil)
        alertController.addAction(ok)
        alertController.addAction(cancel)
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }
}

extension CustomerDetailViewController: QRCodeReaderViewControllerDelegate {
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        reader.stopScanning()
        dismiss(animated: true, completion: nil)
        
        if !result.value.isEmpty {
            let code = result.value
            
            ProductServices.getProductByBarCode(BarCode: code, onSucess: {products in
                if products.count > 0 {
                    APPDELEGATE.stocks.append(products[0])
                    self.inventoryViewController.tableView.reloadData()
                } else {
                    self.tryToGetProductFromLocalDatabase(code: code)
                }
            }, onError: {error in
                print(error)
                self.tryToGetProductFromLocalDatabase(code: code)
            })
            
        }
        
    }
    
    private func tryToGetProductFromLocalDatabase(code: String) {
        let products = REALM.objects(Product.self).filter("barcode = %@", code)
        if products.count > 0 {
            APPDELEGATE.stocks.append(products[0])
            self.inventoryViewController.tableView.reloadData()
            return
        }
        
        showMessage(message: "not_found_offline_product".localized())
    }
    
    func getItemLocalByBarCode(barcode: String) {
        let products = REALM.objects(Product.self).filter("barcode = %@", barcode)
        if products.count > 0 {
            APPDELEGATE.stocks.append(products[0])
            self.inventoryViewController.tableView.reloadData()
        }
    }
    
    func reader(_ reader: QRCodeReaderViewController, didSwitchCamera newCaptureDevice: AVCaptureDeviceInput) {
        if let cameraName = newCaptureDevice.device.localizedName {
            print("Switching capturing to: \(cameraName)")
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
}
