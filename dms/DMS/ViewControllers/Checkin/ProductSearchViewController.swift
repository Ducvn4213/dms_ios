//
//  ProductSearchViewController.swift
//  DMS
//
//  Created by KID on 11/24/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

protocol ProductSearchDelegate {
    func selected(product: Product)
}

class ProductSearchViewController: BaseViewController {
    
    var products: [Product]!
    var filtered: [Product]!
    var delegate: ProductSearchDelegate!
    @IBOutlet weak var tableView: UITableView!
    
    var isPromo = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        filtered = products
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ProductSearchViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productcell") as! ProductViewCell
        cell.labelName.text = filtered[indexPath.row].ItemName
        if isPromo {
            cell.textGiaLe.isHidden = true
            cell.textTonLe.isHidden = true
            cell.textGiaChan.text = products[indexPath.row].ItemCode
            cell.textTonChan.text = products[indexPath.row].CodeBars
        } else {
            cell.textGiaLe.isHidden = false
            cell.textTonLe.isHidden = false
            cell.textGiaChan.text = "Giá Chẵn".localized()
            cell.textTonChan.text = "Tồn Chẵn".localized()
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.selected(product: filtered[indexPath.row])
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

extension ProductSearchViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered = searchText.isEmpty ? products : products.filter { (item: Product) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.ItemName?.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        tableView.reloadData()
    }
}
