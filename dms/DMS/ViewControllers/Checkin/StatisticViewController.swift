//
//  StatisticViewController.swift
//  DMS
//
//  Created by KID on 1/17/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

class StatisticViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    weak var customer: Customer!
    
    var statisticList = [StatisticModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "StatisticHeaderViewCell", bundle: nil), forCellReuseIdentifier: "StatisticHeaderViewCell")
        tableView.register(UINib(nibName: "StatisticViewCell", bundle: nil), forCellReuseIdentifier: "StatisticViewCell")
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 41
        self.title = customer.CardName
        loadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
        LOADING.show()
        CustomerServices.getStatisticFromUser(UserCode: APPDELEGATE.userCode, CardCode: customer.CardCode!, onSucess: {result in
            self.statisticList = result
            self.tableView.reloadData()
            LOADING.dismiss()
        }, onError: {error in
            self.showToast(message: error)
            print(error)
            LOADING.dismiss()
        })
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if (indexPath.row == 0) {
            let cell: StatisticHeaderViewCell = tableView.dequeueReusableCell(withIdentifier: "StatisticHeaderViewCell") as! StatisticHeaderViewCell
            return cell
        } else {
            let cell: StatisticViewCell = tableView.dequeueReusableCell(withIdentifier: "StatisticViewCell") as! StatisticViewCell
            let statistic = statisticList[indexPath.row - 1]
            cell.labelNum.text = String(indexPath.row)
            if statistic.CheckInTime.isEmpty {
                cell.labelCheckin.textColor = UIColor.red
                cell.labelCheckin.text = "N/A"
            } else {
                cell.labelCheckin.textColor = UIColor.black
                cell.labelCheckin.text = statistic.CheckInTime.stringToDateTimeString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSz", outputFormat: "dd/MM/yyyy HH:mm")
            }
            if statistic.CheckOutTime.isEmpty {
                cell.labelCheckout.textColor = UIColor.red
                cell.labelCheckout.text = "N/A"
            } else {
                cell.labelCheckout.textColor = UIColor.black
                cell.labelCheckout.text = statistic.CheckOutTime.stringToDateTimeString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSSz", outputFormat: "dd/MM/yyyy HH:mm")
            }
            if statistic.NoCustOrder.isEmpty {
                cell.labelOrder.textColor = UIColor.red
                cell.labelOrder.text = "N/A"
            } else {
                cell.labelOrder.textColor = UIColor.black
                cell.labelOrder.text = statistic.NoCustOrder
            }
            if statistic.NoPicTake.isEmpty {
                cell.labelPic.textColor = UIColor.red
                cell.labelPic.text = "N/A"
            } else {
                cell.labelPic.textColor = UIColor.black
                cell.labelPic.text = statistic.NoPicTake
            }
            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return statisticList.count + 1
    }

}
