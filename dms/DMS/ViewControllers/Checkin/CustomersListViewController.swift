//
//  CustomersListViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import MagicalRecord
import GoogleMaps
import PopupDialog

class CustomersListViewController: BaseViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var locationManager = CLLocationManager()
    
    var currentLocation: CLLocation!
    
    var customers: [Customer] = []
    
    var filter: CustomerFilter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.distanceFilter = 50
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        loadData()
        filter = CustomerFilter(Route: 0, Channel: 0, Group: 0, FreeText: "", sort: 0)
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 230.0
    }
    
    func loadData() {
        self.customers = REALM.objects(Customer.self).toArray(ofType: Customer.self)
        self.tableView.reloadData()
    }
    
    @IBAction func actionAdd(_ sender: Any) {
        let vs = UIStoryboard.addCustomerViewController()
        vs.result = {(result) in
            if(result) {
                let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "00"
                let tChannel = self.filter.Channel > 0 ? REALM.objects(Channel.self)[self.filter.Channel - 1].ChannelCode! : "00"
                let tGroup = self.filter.Group > 0 ? REALM.objects(CustomerGroup.self)[self.filter.Group - 1].GrpCode! : "00"
                
                self.searchCustomer(Route: tRouter, Channel: tChannel, Group: tGroup, FreeText: self.searchBar.text!, replace: true)
            }
        }
        navigationController?.pushViewController(vs, animated: true)
    }
    
    @IBAction func actionFilter(_ sender: Any) {
        let vs = CustomerFilterViewController(nibName: "CustomerFilterViewController", bundle: nil)
        
        vs.filter = self.filter
        
        // Create the dialog
        let p = PopupDialog(viewController: vs, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Pass a reference to the dialog
        vs.dialog = p
        
        vs.search = { (router, sort, type, group) in
            p.dismiss()
            
            self.filter = CustomerFilter(Route: router, Channel: type, Group: group, FreeText: self.searchBar.text!, sort: sort)
            
            let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "00"
            let tChannel = self.filter.Channel > 0 ? REALM.objects(Channel.self)[self.filter.Channel - 1].ChannelCode! : "00"
            let tGroup = self.filter.Group > 0 ? REALM.objects(CustomerGroup.self)[self.filter.Group - 1].GrpCode! : "00"
            
            self.searchCustomer(Route: tRouter, Channel: tChannel, Group: tGroup, FreeText: self.searchBar.text!)
        }
        
        self.present(p, animated: true, completion: nil)
    }
    
    deinit {
        print("shit")
    }
}

extension CustomersListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.customers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerTableViewCell", for: indexPath) as! CustomerTableViewCell
        let customer = self.customers[indexPath.row]
        cell.setupWithCustomer(customer: customer)
        cell.delegate = self
        cell.selectionStyle = .none
        return cell
    }
}

extension CustomersListViewController: CustomerTableViewCellDelegate {
    func statistic(customer: Customer) {
        let vc = UIStoryboard.statisticViewController()
        vc.customer = customer
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func book(customer: Customer) {
        let vc = UIStoryboard.newOrderViewController()
        vc.customer = customer
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func checkinWithCustomer(customer: Customer) {
        
        let cusLocation = CLLocation(latitude: Double(customer.LatitudeValue!)!, longitude: Double(customer.LongitudeValue!)!)
        
        let distance = cusLocation.distance(from: currentLocation)
        
        if (distance > 200) {
            
            let dialog = UIAlertController(title: "iDMS Management", message: "Khoảng cách chênh lệch: \(Int(distance)) m", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .default, handler: {(alert : UIAlertAction!) in
                self.gotoCheckin(customer: customer)
            })
            
            dialog.addAction(action)
            self.present(dialog, animated: true, completion: nil)
        } else {
            
            gotoCheckin(customer: customer)
        }
    }
    
    func note(customer: Customer) {
        LOADING.show()
        CustomerServices.getNoteFromUser(UserCode: APPDELEGATE.userCode, CardCode: customer.CardCode!, onSucess: { (data) in
            LOADING.dismiss()
            
            let vs = NoteViewController(nibName: "NoteViewController", bundle: nil)
            
            vs.name = customer.CardName!
            vs.message = data
            
            let p = PopupDialog(viewController: vs)
            
            self.present(p, animated: true, completion: nil)
            
        }, onError: { (error) in
            LOADING.dismiss()
            self.showMessage(message: error)
        })
        
    }
    
    func ship() {
        self.showMessage(message: notImplementMessage)
    }
    
    func gotoCheckin(customer: Customer) {
        //        markVisited(customer: customer)
        if let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomerDetailViewController") as? CustomerDetailViewController {
            vc.customer = customer
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func map(customer : Customer) {
        let vc = UIStoryboard.checkinMapViewController()
        vc.customer = customer
        vc.title = customer.CardName
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    //    func markVisited(customer:Customer) {
    //        let visitedDate = Date()
    //
    //        let index = self.filtered.index() { $0.CardCode == customer.CardCode }
    //        let row = index?.hashValue ?? 0
    //        self.filtered[row].VisitStatus = true
    //        self.filtered[row].visitedDate = visitedDate
    //        self.tableView.reloadRows(at: [IndexPath(row: row, section: 0)], with: .automatic)
    //
    //        let visitedCustomer = VisitedCustomer.mr_createEntity()
    //        visitedCustomer?.code = customer.code
    //        visitedCustomer?.visitedDate = visitedDate
    //
    //        NSManagedObjectContext.mr_default().mr_saveToPersistentStore() {(success, error) in
    //            if success {
    //                print("COMPLETED SAVE")
    //            } else if (error != nil) {
    //                print("Error saving context: %@", error ?? "unknow")
    //            }
    //        }
    //    }
}

extension CustomersListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        //        filtered = searchText.isEmpty ? customers : customers.filter { (item: Customer) -> Bool in
        //            // If dataItem matches the searchText, return true to include it
        //            return item.CardName?.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        //        }
        let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "00"
        let tChannel = self.filter.Channel > 0 ? REALM.objects(Channel.self)[self.filter.Channel - 1].ChannelCode! : "00"
        let tGroup = self.filter.Group > 0 ? REALM.objects(CustomerGroup.self)[self.filter.Group - 1].GrpCode! : "00"
        
        searchCustomer(Route: tRouter, Channel: tChannel, Group: tGroup, FreeText: searchText)
        //        tableView.reloadData()
    }
    
    func searchCustomer(Route: String = "00", Channel: String = "00", Group: String = "00", FreeText: String = "", replace: Bool = false) {
        CustomerServices.searchCustomers(UserCode: APPDELEGATE.userCode, Route: Route, Channel: Channel, Group: Group, SalesManId: APPDELEGATE.SalesManId, FreeText: FreeText, WorkingDate: APPDELEGATE.WorkingDate, onSucess: {
            (customers) in
            if (replace) {
                try! REALM.write {
                    REALM.delete(REALM.objects(Customer.self))
                    for c in customers {
                        REALM.add(c)
                    }
                    self.loadData()
                }
            } else {
                self.customers = customers
                self.tableView.reloadData()
            }
        }) {
            (error) in
            print(error)
        }
    }
}

extension CustomersListViewController: CLLocationManagerDelegate {
    
    // Handle incoming location events.
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location: CLLocation = locations.last!
        
        print("Location: \(location)")
        
        self.currentLocation = location
        
        locationManager.stopUpdatingLocation()
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
        // Display the map using the default location.
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
