//
//  NewOrderViewController.swift
//  DMS
//
//  Created by KID on 12/20/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import RealmSwift

class NewOrderViewController: BaseViewController {

    @IBOutlet weak var btnAddProduct: UIButton!
    @IBOutlet weak var btnKhuyenMai: UIButton!
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint:
    NSLayoutConstraint!
    
    @IBOutlet weak var labelCustomer: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelGroup: UILabel!
    
    @IBOutlet weak var txtDeliveryDate: UITextField!
    
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var dropDownView: MyDropDownView!
    
    var listItem = List<ItemOrder>()
    
    var loadCount = 0
    
    let datePickerView:UIDatePicker = UIDatePicker()
    
    weak var customer: Customer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.btnAddProduct.alignImageAndTitleVertically(padding: 6)
        self.btnKhuyenMai.alignImageAndTitleVertically(padding: 6)
        
        self.btnUpdate.alignImageAndTitleVertically(padding: 6)
        
        self.tableView.estimatedRowHeight = 275.0
        self.tableView.rowHeight = UITableViewAutomaticDimension

        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        self.tableView.register(UINib(nibName: "OrderViewCell", bundle: nil), forCellReuseIdentifier: "OrderViewCell")
        
        loadData()
        // Do any additional setup after loading the view.
    }
    
    deinit {
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        tableHeightConstraint.constant = tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.tableHeightConstraint.constant = self.tableView.contentSize.height
            self.tableView.layoutIfNeeded()
        }
    }
    
    func loadData() {
        labelCustomer.text = customer.CardName
        labelPhone.text = customer.Tel
        
        for item in REALM.objects(CustomerGroup.self) {
            if item.GrpCode == customer.CustGrp {
                labelGroup.text = item.GrpName
            }
        }
        
        var wares = [String]()
        
        for item in REALM.objects(WareHouse.self) {
            wares.append(item.WhsName!)
        }
        
        dropDownView.dropDownView.dataSource = wares
        
        try! REALM.write {
            REALM.delete(REALM.objects(ProductPrice.self))
        }
        LOADING.show()
        
        getProductPrice(REALM.objects(Product.self).toArray(ofType: Product.self))
        ProductServices.getPromoProducts(cusCode: customer.CardCode!, UserCode: APPDELEGATE.userCode, CurrDate: Date().toStringMMDDYYYY(), onSucess: {(products) in
            APPDELEGATE.promoProducts = products
            self.loadCount += 1
            if self.loadCount == 2 {
                LOADING.dismiss()
            }
        }, onError: {(error) in
            print(error)
            self.loadCount += 1
            if self.loadCount == 2 {
                LOADING.dismiss()
            }
        })
    }
    
    
    @IBAction func actionDTime(_ sender: UITextField) {
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        if (sender.text! != "") {
            datePickerView.date = dateFormatter.date(from: sender.text!)!
        }
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        sender.inputAccessoryView = toolBar
    }
    
    func donePicker() {
        txtDeliveryDate.text = datePickerView.date.toStringMMDDYYYY()
        txtDeliveryDate.endEditing(true)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {

        txtDeliveryDate.text = sender.date.toStringMMDDYYYY()
        
    }
    @IBAction func actionAdd(_ sender: Any) {
        let vc = UIStoryboard.orderProductViewController()
        vc.products = REALM.objects(Product.self).toArray(ofType: Product.self)
        
        var checkList: [String] = []
        
        for item in listItem {
            checkList.append((item.item?.ItemCode)!)
        }
        
        vc.checkList = checkList
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func getProductPrice(_ listProduct: [Product]) {
        
        if (listProduct.count == 0) {
            loadCount += 1
            if loadCount == 2 {
                LOADING.dismiss()
            }
            return
        }
        let product = listProduct[0]
        ProductServices.getProductPrice(CustCode: customer.CardCode!, ItemCode: product.ItemCode!, UoM: product.BuyUoM!, CurrDate: Date().toStringMMDDYYYY(), onSucess: {(price, vat) in
            var results = listProduct
            let productPrice = ProductPrice()
            productPrice.product = product
            productPrice.price = price
            productPrice.vat = vat
            try! REALM.write {
                REALM.add(productPrice)
            }
            results.remove(at: 0)
            self.getProductPrice(results)
        }, onError: {(error) in
            try! REALM.write {
                REALM.delete(REALM.objects(ProductPrice.self))
            }
            LOADING.dismiss()
            self.showToast(message: "Can't get product price")
            self.navigationController?.popViewController(animated: true)
        })
    }
    @IBAction func actionPromo(_ sender: Any) {
        let vc = UIStoryboard.productSearchViewController()
        vc.products = APPDELEGATE.promoProducts
        vc.delegate = self
        vc.isPromo = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionUpdate(_ sender: Any) {
        
        if (txtDeliveryDate.text?.isEmpty)! {
            showToast(message: "Vui lòng chọn ngày giao hàng".localized())
            return
        }
        
        if dropDownView.dropDownView.indexForSelectedRow == nil {
            showToast(message: "Vui lòng chọn kho xuất".localized())
            return
        }
        
        if listItem.count == 0 {
            showToast(message: "Vui lòng chọn sản phẩm".localized())
            return
        }
        showDialog(title: "iDMS Management".localized(), message: "Bạn có chắc chắn muốn đặt đơn hàng này không?".localized(), handlerOK: {(UIAlertAction) in
            let UserCode = APPDELEGATE.userCode
            let Cardcode = self.customer.CardCode
            let OrderDate = Date().toStringMMDDYYYY()
            let DeliveryDate = self.txtDeliveryDate.text
            let CardName = self.customer.CardName
            LOADING.show()

            let newOrder = AddOrder()

            newOrder.Cardcode = Cardcode
            newOrder.OrderDate = OrderDate
            newOrder.DeliveryDate = DeliveryDate
            newOrder.CardName = CardName
            newOrder.orderList = self.listItem
            
            let syncObj = SyncObject()
            
            syncObj.SyncType = sync_order_type
            syncObj.Order = newOrder

            try! REALM.write {
                REALM.add(syncObj)
            }
            
            SyncService.sharedInstance.processSyncWith(syncObjects: [syncObj], onSucess: {(string) in
                LOADING.dismiss()
                self.showDialog(title: "Order", message: "Update order complete".localized(), handlerOK: {(UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                })
                
            }, onError: {(string) in
                LOADING.dismiss()
                self.showToast(message: string)
                self.showDialog(title: "Error", message: "Có lỗi, order đã được thêm offline".localized(), handlerOK: {(UIAlertAction) in
                    self.navigationController?.popViewController(animated: true)
                })
            })
            
//            self.addOrder(list: self.listItem, UserCode: UserCode!, Cardcode: Cardcode!, OrderDate: OrderDate, DeliveryDate: DeliveryDate!, CardName: CardName!)
        }, handlerCancel: {(UIAlertAction) in
            
        })
    }
    
//    func addOrder(list : [ItemOrder], UserCode: String, Cardcode: String, OrderDate: String, DeliveryDate: String, CardName: String) {
//        if list.count == 0 {
//            LOADING.dismiss()
//            showDialog(title: "Order".localized(), message: "Update order complete".localized(), handlerOK: {(UIAlertAction) in
//                self.navigationController?.popViewController(animated: true)
//            })
//            return
//        }
//
//        var newList: [ItemOrder] = list
//
//        OrderServices.addOrder(UserCode: UserCode, Cardcode: Cardcode, OrderDate: OrderDate, DeliveryDate: DeliveryDate, CardName: CardName, ItemCode: (newList[0].item?.ItemCode)!, ItemName: (newList[0].item?.ItemName)!, ItemType: newList[0].ItemType!, Quantity: newList[0].Quantity!, Uom: (newList[0].item?.BuyUoM)!, Price: newList[0].Price!, VAT: newList[0].VAT!, onSucess: {(id) in
//
//            newList.remove(at: 0)
//            self.addOrder(list: newList, UserCode: UserCode, Cardcode: Cardcode, OrderDate: OrderDate, DeliveryDate: DeliveryDate, CardName: CardName)
//        }, onError: {(error) in
//            self.showToast(message: "Update order error".localized())
//        })
//    }
    
    
    func calTotal() {
        var total = 0
        
        for item in listItem {
            
            let price = (item.Price == nil || (item.Price?.isEmpty)!) ? 0 : Int(item.Price!)
            
            let quantity = (item.Quantity == nil || (item.Quantity?.isEmpty)!) ? 0 : Int(item.Quantity!)
            
            total += price! * quantity!
        }
        
        labelTotal.text = String(total)
        self.title = "Tổng tiền: " + String(total)
    }
    
}

extension NewOrderViewController: UITableViewDelegate, UITableViewDataSource, OrderItemDelegate {
    func amountChange(_ amount: String, sender: UITextField) {
        let cell: UITableViewCell = sender.superview?.superview?.superview as! UITableViewCell
        let indexPath = tableView.indexPath(for: cell)
        listItem[(indexPath?.row)!].Quantity = amount
        calTotal()
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderViewCell") as! OrderViewCell
        
        let product = REALM.objects(Product.self).filter("ItemCode = %@", listItem[indexPath.row].item?.ItemCode as Any).first
        
        cell.labelName.text = product?.ItemName
        cell.labelCode.text = product?.CodeBars
        cell.labelUnit.text = product?.BuyUoM
        cell.labelVat.text = product?.InvUoM
        cell.txtAmount.text = listItem[indexPath.row].Quantity
        
        cell.labelPrice.text = listItem[indexPath.row].Price
        cell.labelVat.text = listItem[indexPath.row].Quantity
        cell.delegate = self
        
        cell.labelType.text = (listItem[indexPath.row].ItemType == "0") ? "0(Hàng bán)" : "1(Hàng khuyến mãi)"
        
//        cell.labelPrice.text = product?.price
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
}

extension NewOrderViewController: AddProductViewControllerDelegate, ProductSearchDelegate {
    func selected(product: Product) {
        for item in listItem {
            if(item.item?.ItemCode == product.ItemCode) {
                
                let alertController = UIAlertController(title: "Có lỗi".localized(), message: "Không thể chọn sản phẩm đã tồn tại".localized(), preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                alertController.addAction(ok)
                
                self.navigationController?.present(alertController, animated: true, completion: nil)
                return
            }
        }
        
        let alertController = UIAlertController(title: "Thêm sản phẩm".localized(), message: "Bạn có chắc chắn muốn thêm sản phẩm này không".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: {(UIAlertAction) in
            
            let newOrder = ItemOrder()
            newOrder.item = REALM.objects(Product.self).filter("ItemCode = %@", product.ItemCode as Any).first
            newOrder.Quantity = "0"
            self.listItem.append(newOrder)
            self.tableView.reloadData()
            self.navigationController?.popViewController(animated: true)
        })
        let cancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel, handler: nil)
        alertController.addAction(ok)
        alertController.addAction(cancel)
        self.navigationController?.present(alertController, animated: true, completion: nil)
    }
    
    func Apply(checkList: [String]) {
        print(checkList)
        
        var newList = List<ItemOrder>()
        
        for itemCode in checkList {
            var isExist = false
            for item in listItem {
                if item.item?.ItemCode == itemCode {
                    newList.append(item)
                    isExist = true
                }
            }
            if !isExist {
                let newOrder = ItemOrder()
                newOrder.item = REALM.objects(Product.self).filter("ItemCode = %@", itemCode).first
                newOrder.Quantity = "0"
                let productPrice = REALM.objects(ProductPrice.self).filter("product.ItemCode = %@", itemCode).first
                newOrder.Price = productPrice?.price
                newOrder.VAT = productPrice?.vat
                
                for item in APPDELEGATE.promoProducts {
                    newOrder.ItemType = String(item.ItemCode == itemCode ? 1 : 0)
                }
                
                newList.append(newOrder)
            }
        }
        
        self.listItem = newList
        tableView.reloadData()
        calTotal()
        self.navigationController?.popViewController(animated: true)
    }
    

}
