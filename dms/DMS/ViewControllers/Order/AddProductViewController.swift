//
//  AddProductViewController.swift
//  DMS
//
//  Created by KID on 1/3/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

protocol AddProductViewControllerDelegate {
    func Apply(checkList: [String])
}

class AddProductViewController: BaseViewController {
    
    var products: [Product]!
    var filtered: [Product]!
    var delegate: AddProductViewControllerDelegate!
    var checkList: [String]!
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        filtered = products
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionApply(_ sender: Any) {
        delegate.Apply(checkList: checkList)
    }
}

extension AddProductViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filtered.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderproductcell") as! OrderProductViewCell
        cell.labelName.text = filtered[indexPath.row].ItemName
        let productPrice = REALM.objects(ProductPrice.self).filter("product.ItemCode = %@", products[indexPath.row].ItemCode as Any).first
        cell.labelGia.text = productPrice?.price
        cell.labelVAT.text = productPrice?.vat        
        
        if checkList.contains(filtered[indexPath.row].ItemCode!) {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        } else {
            cell.accessoryType = UITableViewCellAccessoryType.none
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let currentCell = tableView.cellForRow(at: indexPath as IndexPath)!
        if currentCell.accessoryType == UITableViewCellAccessoryType.checkmark {
            currentCell.accessoryType = UITableViewCellAccessoryType.none
            checkList = checkList.filter() { $0 != filtered[indexPath.row].ItemCode }
        } else {
            currentCell.accessoryType = UITableViewCellAccessoryType.checkmark
            checkList.append(filtered[indexPath.row].ItemCode!)
        }
    }
}

extension AddProductViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filtered = searchText.isEmpty ? products : products.filter { (item: Product) -> Bool in
            // If dataItem matches the searchText, return true to include it
            return item.ItemName?.range(of: searchText, options: .caseInsensitive, range: nil, locale: nil) != nil
        }
        tableView.reloadData()
    }
}
