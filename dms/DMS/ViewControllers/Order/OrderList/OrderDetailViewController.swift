//
//  OrderDetailViewController.swift
//  DMS
//
//  Created by KID on 1/23/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

protocol OrderDetailDelegate {
    func didCancel()
    func didShip()
}

class OrderDetailViewController: BaseViewController {
    
    @IBOutlet weak var labelCustomer: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelContact: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var cash: UILabel!
    @IBOutlet weak var LabelDeliverytime: UILabel!
    @IBOutlet weak var orderType: MyDropDownView!
    @IBOutlet weak var wareHouse: MyDropDownView!
    @IBOutlet weak var labelPromo: UILabel!
    @IBOutlet weak var labelNote: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnShip: UIButton!
    
    weak var order: Order!
    var delegate: OrderDetailDelegate!
    var customer: Customer!
    var listItem = [OrderItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.estimatedRowHeight = 214.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        self.tableView.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        
        self.tableView.register(UINib(nibName: "OrderEditViewCell", bundle: nil), forCellReuseIdentifier: "OrderEditViewCell")
        
        self.btnCancel.alignImageAndTitleVertically(padding: 6)
        self.btnShip.alignImageAndTitleVertically(padding: 6)
        loadData()
    }
    
    deinit {
        self.tableView.removeObserver(self, forKeyPath: "contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        tableView.layer.removeAllAnimations()
        tableHeightConstraint.constant = tableView.contentSize.height
        UIView.animate(withDuration: 0.5) {
            self.tableHeightConstraint.constant = self.tableView.contentSize.height
            self.tableView.layoutIfNeeded()
        }
    }
    
    func loadData() {
        LOADING.show()
        customer = REALM.objects(Customer.self).filter("CardCode=%@", order.CardCode as Any).first
        labelCustomer.text = order.CardName
        labelAddress.text = order.Address
        if customer != nil {
            labelContact.text = customer.ContactPerson
            labelPhone.text = customer.Tel
        } else {
            labelContact.text = "N/A"
            labelPhone.text = "N/A"
        }
        cash.text = order.DocTotal
        LabelDeliverytime.text = order.DeliveryDate?.stringToDateTimeString(inputFormat: "yyyy-MM-dd'T'HH:mm:ssz", outputFormat: "yyyy/MM/dd")
        labelPromo.text = order.DocGToal
        labelNote.text = order.Remark
        
        var wares = [String]()
        
        for item in REALM.objects(WareHouse.self) {
            wares.append(item.WhsName!)
        }
        wareHouse.dropDownView.dataSource = wares
        wareHouse.setDefault()
        
        var orderTypes = [String]()
        
        for item in REALM.objects(TypeOrder.self) {
            orderTypes.append(item.TypeName!)
        }
        orderType.dropDownView.dataSource = orderTypes
        orderType.setDefault(index: Int(order.OrderType!)!)
        
        OrderServices.getOrderByDocEntry(DocEntry: order.DocEntry!, onSucess: { (orderItems) in
            self.listItem = orderItems
            self.tableView.reloadData()
            LOADING.dismiss()
        }) { (error) in
            self.showToast(message: error)
            print(error)
            LOADING.dismiss()
        }
        
    }
    
    @IBAction func actionCancel(_ sender: Any) {
        
        LOADING.show()
        OrderServices.cancelOrder(UserCode: APPDELEGATE.userCode, DocEntry: order.DocEntry!, onSucess: { (result, mesage) in
            LOADING.dismiss()
            if result == "1" {
                self.showDialog(title: "Cancel order".localized(), message: mesage, handlerOK: { (UIAlertAction) in
                    self.delegate.didCancel()
                }, handlerCancel: { (UIAlerAction) in
                    
                })
            } else {
                self.showToast(message: mesage)
            }
        }) { (error) in
            self.showToast(message: error)
            print(error)
            LOADING.dismiss()
        }
        
    }
    @IBAction func actionShip(_ sender: Any) {
        self.showDialog(title: "Ship order".localized(), message: "Update delivery complete".localized(), handlerOK: { (UIAlertAction) in
            self.delegate.didShip()
        }, handlerCancel: { (UIAlerAction) in
            
        })
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension OrderDetailViewController: UITableViewDelegate, UITableViewDataSource, OrderItemDelegate {
    func amountChange(_ amount: String, sender: UITextField) {
        let cell: UITableViewCell = sender.superview?.superview?.superview as! UITableViewCell
        let indexPath = tableView.indexPath(for: cell)
        listItem[(indexPath?.row)!].Quantity = amount
//        calTotal()
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderEditViewCell") as! OrderEditViewCell
        
        let item = listItem[indexPath.row]
        
        cell.labelName.text = item.ItemName
        cell.labelUnit.text = item.UoM
        cell.labelPrice.text = item.Price
        cell.labelPromo.text = item.Discount
        cell.txtAmount.text = item.Quantity
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listItem.count
    }
}
