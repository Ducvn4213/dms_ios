//
//  OrderListViewController.swift
//  DMS
//
//  Created by KID on 1/22/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit
import RealmSwift
import PopupDialog

class OrderListViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var orders = [Order]()
    
    var filter: OrderFilter!
    var filterString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.filter = OrderFilter(Route: 0, status: 0, statusCheckin: 0, FreeText: "", fromTime: "2016/01/01", toTime: Date().toStringYYYYMMDD(), type: 0)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 215
        loadData()
    }
    
    func loadData() {
        self.orders = REALM.objects(Order.self).toArray(ofType: Order.self)
        self.tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionFilter(_ sender: Any) {
        let vs = OrderFilterViewController(nibName: "OrderFilterViewController", bundle: nil)
        
        vs.filter = self.filter
        
        // Create the dialog
        let p = PopupDialog(viewController: vs, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        // Pass a reference to the dialog
        vs.dialog = p
        
        vs.search = { (router, status, type, statusCheckin, from, to)  in
            p.dismiss()
            
            self.filter = OrderFilter(Route: router, status: status, statusCheckin: statusCheckin, FreeText: self.searchBar.text!, fromTime: from, toTime: to, type: type)
            
            let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "01"
            let tStatus = self.filter.status > 0 ? REALM.objects(StatusOrder.self)[self.filter.status - 1].StatusCode! : "01"
            let tStatusCheckin = self.filter.statusCheckin > 0 ? REALM.objects(TypeVisit.self)[self.filter.statusCheckin - 1].StatusCode! : "01"
            let tType = self.filter.type > 0 ? REALM.objects(TypeOrder.self)[self.filter.type - 1].TypeCode! : "01"
            
            self.searchOrder(FromDate: self.filter.fromTime, ToDate: self.filter.toTime, DocStatus: tStatus, VisitStatus: tStatusCheckin, OrderType: tType, Route: tRouter, FreeText: self.searchBar.text!, replace: false)
        }
        
        self.present(p, animated: true, completion: nil)
    }
    
    func doUpdateOrder(order : Order) {
        let vs = OrderUpdateViewController(nibName: "OrderUpdateViewController", bundle: nil)
        
        let p = PopupDialog(viewController: vs, buttonAlignment: .horizontal, transitionStyle: .bounceDown, gestureDismissal: true)
        
        vs.dialog = p
        vs.parentContainer = self
        vs.orderData = order

        self.present(p, animated: true, completion: nil)
    }
    
    func doCancelOrder(docEntry: String) {
        LOADING.show()
        OrderServices.cancelOrder(UserCode: APPDELEGATE.userCode, DocEntry: docEntry, onSucess: { (result, mesage) in
            LOADING.dismiss()
            if (mesage.contains("Unsuccessfully")) {
                self.showToast(message: mesage)
            }
            else {
                let orders = REALM.objects(Order.self).filter("DocEntry = %@", docEntry)
                
                let realm = try! Realm()
                if let order = orders.first {
                    try! realm.write {
                        order.DocStatus = "06"
                        order.DocStatusName = "Huỷ"
                    }
                }
                
                self.showToast(message: mesage)
                self.loadData()
            }
        }) { (error) in
            self.showToast(message: error)
            print(error)
            LOADING.dismiss()
        }
    }
    
    func doUpdateOrder(status: String, date: String, note: String, order: Order) {
        LOADING.show()
        OrderServices.updateOrder(userCode: APPDELEGATE.userCode, docEntry: order.DocEntry!, date: date, status: status, note: note, onSucess: { (result) in
            LOADING.dismiss()
            let orders = REALM.objects(Order.self).filter("DocEntry = %@", order.DocEntry!)
            
            let realm = try! Realm()
            if let order = orders.first {
                try! realm.write {
                    order.DocStatus = status
                    order.DeliveryDate_Act = getCurrentISOTime()
                    order.DeliveryDate = normalTimeToISOTime(input: date)
                    order.Remark = note
                }
            }
            
            self.showToast(message: result)
            
            if (self.filterString.isEmpty) {
                self.loadData()
            }
            else {
                self.orders = REALM.objects(Order.self).filter(self.self.filterString).toArray(ofType: Order.self)
                self.tableView.reloadData()
            }
            
        }) { (error) in
            self.showToast(message: error)
            print(error)
            LOADING.dismiss()
        }
    }
}

extension OrderListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let order = orders[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderlistcell") as! OrderListViewCell
        
        cell.setData(data: order)
        cell.containerViewCotroller = self
        
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        let detail = UIStoryboard.orderDetailViewController()
        detail.order = orders[indexPath.row]
        detail.delegate = self
        self.navigationController?.pushViewController(detail, animated: true)
    }
}

extension OrderListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "01"
        let tStatus = self.filter.status > 0 ? REALM.objects(StatusOrder.self)[self.filter.status - 1].StatusCode! : "01"
        let tStatusCheckin = self.filter.statusCheckin > 0 ? REALM.objects(TypeVisit.self)[self.filter.statusCheckin - 1].StatusCode! : "01"
        let tType = self.filter.type > 0 ? REALM.objects(TypeOrder.self)[self.filter.type - 1].TypeCode! : "01"
        
        searchOrder(FromDate: filter.fromTime, ToDate: filter.toTime, DocStatus: tStatus, VisitStatus: tStatusCheckin, OrderType: tType, Route: tRouter, FreeText: searchBar.text!, replace: false)
    }
    
    func searchOrder(FromDate: String = "2016/01/01", ToDate: String, DocStatus: String = "01", VisitStatus: String = "01", OrderType: String = "01", Route: String = "01", FreeText: String = "", replace: Bool = false) {
        
        if (DocStatus != "01") {
            filterString = filterString + "DocStatus = '" + DocStatus + "'"
        }
        
        if (OrderType != "01") {
            filterString = filterString + " AND OrderType = '" + OrderType + "'"
        }
        
        if (Route != "01") {
            filterString = filterString + " AND Route = '" + Route + "'"
        }
        
        if (filterString.isEmpty) {
            loadData()
        }
        else {
            self.orders = REALM.objects(Order.self).filter(filterString).toArray(ofType: Order.self)
            self.tableView.reloadData()
        }
    }
}

extension OrderListViewController: OrderDetailDelegate {
    func didCancel() {
        let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "01"
        let tStatus = self.filter.status > 0 ? REALM.objects(StatusOrder.self)[self.filter.status - 1].StatusCode! : "01"
        let tStatusCheckin = self.filter.statusCheckin > 0 ? REALM.objects(TypeVisit.self)[self.filter.statusCheckin - 1].StatusCode! : "01"
        let tType = self.filter.type > 0 ? REALM.objects(TypeOrder.self)[self.filter.type - 1].TypeCode! : "01"
        searchOrder(FromDate: filter.fromTime, ToDate: filter.toTime, DocStatus: tStatus, VisitStatus: tStatusCheckin, OrderType: tType, Route: tRouter, FreeText: searchBar.text!, replace: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func didShip() {
        let tRouter = self.filter.Route > 0 ? REALM.objects(Route.self)[self.filter.Route - 1].RouteCode! : "01"
        let tStatus = self.filter.status > 0 ? REALM.objects(StatusOrder.self)[self.filter.status - 1].StatusCode! : "01"
        let tStatusCheckin = self.filter.statusCheckin > 0 ? REALM.objects(TypeVisit.self)[self.filter.statusCheckin - 1].StatusCode! : "01"
        let tType = self.filter.type > 0 ? REALM.objects(TypeOrder.self)[self.filter.type - 1].TypeCode! : "01"
        searchOrder(FromDate: filter.fromTime, ToDate: filter.toTime, DocStatus: tStatus, VisitStatus: tStatusCheckin, OrderType: tType, Route: tRouter, FreeText: searchBar.text!, replace: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
