import UIKit

class ChangePasswordViewController : BaseViewController {
    @IBOutlet weak var txtOldPass: UITextField!
    @IBOutlet weak var txtNewPass: UITextField!
    @IBOutlet weak var txtConfirm: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onChangeClicked(_ sender: Any) {
        let oldPass = txtOldPass.text
        let newPass = txtNewPass.text
        let confirm = txtConfirm.text
        
        if (oldPass?.isEmpty)! {
            let title = "dialog_title_error".localized()
            let messa = "change_password_missing_old_password".localized()
            showDialog(title: title, message: messa)
            return
        }
    
        if (newPass != confirm) {
            let title = "dialog_title_error".localized()
            let messa = "change_password_the_password_not_match".localized()
            showDialog(title: title, message: messa)
            return
        }
        
        if (newPass?.isEmpty)! {
            let title = "dialog_title_error".localized()
            let messa = "change_password_missing_new_password".localized()
            showDialog(title: title, message: messa)
            return
        }
        
        LOADING.show()
        LoginServices.changePassword(userCode: APPDELEGATE.userCode, oldPassword: oldPass!, newPassword: newPass!, onSucess: { (result) in
            LOADING.dismiss()
            
            self.txtOldPass.text = ""
            self.txtNewPass.text = ""
            self.txtConfirm.text = ""
            
            let title = "dialog_title_error".localized()
            let messa = "change_password_success".localized()
            self.showDialog(title: title, message: messa)
        }) { (error) in
            LOADING.dismiss()
            self.showMessage(message: error)
        }
        
    }
}
