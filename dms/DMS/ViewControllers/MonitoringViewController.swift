//
//  MonitoringViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import MXSegmentedPager

class MonitoringViewController: MXSegmentedPagerController {
    
    var staffListVc: MonintorStaffListViewController!
    var mapVc: MonitorMapViewController!
    var loadstatus: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let sb = UIStoryboard(name: "Monitor", bundle: nil)
        staffListVc = sb.instantiateViewController(withIdentifier: "MonintorStaffListViewController") as! MonintorStaffListViewController
        staffListVc.delegate = self
        mapVc = sb.instantiateViewController(withIdentifier: "MonitorMapViewController") as! MonitorMapViewController

        setupSegmentedPager()
        loadData()
    }
    
    func loadData() {
        LOADING.show()
        let date = Date().toStringMMDDYYYY()
        MonitorServices.getStaffs(updateDate: date, onSucess: { (staffs) in
            APPDELEGATE.staffs = staffs
            self.loadstatus += 1
            if (self.loadstatus == 2) {
                self.staffListVc.loadData()
                self.mapVc.loadData()
                LOADING.dismiss()
            }
        }) { (error) in
            print(error)
        }
        MonitorServices.getAllStaffsLocation(onSucess: { (staffPositions) in
            APPDELEGATE.sLocations = staffPositions
            self.loadstatus += 1
            if (self.loadstatus == 2) {
                self.staffListVc.loadData()
                self.mapVc.loadData()
                LOADING.dismiss()
            }
        }, onError: { (error) in
            print(error)
        })
    }

    func setupSegmentedPager() {
        self.segmentedPager.segmentedControl.selectionIndicatorLocation = .down
        self.segmentedPager.segmentedControl.backgroundColor = UIColor.white
        //        self.segmentedPager.segmentedControl.selectionIndicatorColor = UIColor(hexString: "9B6021")
        self.segmentedPager.segmentedControl.selectionIndicatorHeight = 3
        self.segmentedPager.segmentedControl.selectionStyle = .fullWidthStripe
        self.segmentedPager.segmentedControl.titleTextAttributes = [NSFontAttributeName: UIFont.systemFont(ofSize: 13),
                                                                    NSForegroundColorAttributeName: UIColor.darkText]
    }
}

extension MonitoringViewController {
    override func numberOfPages(in segmentedPager: MXSegmentedPager) -> Int {
        return 2
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, titleForSectionAt index: Int) -> String {
        if index == 0 {
            return "Danh sách"
        } else {
            return "Bản đồ"
        }
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, viewForPageAt index: Int) -> UIView {
        if index == 0 {
            
            addChildViewController(staffListVc)
            
            return staffListVc.view
        } else {
            addChildViewController(mapVc)
            return mapVc.view
        }
    }
    
    override func segmentedPager(_ segmentedPager: MXSegmentedPager, didSelectViewWith index: Int) {
        if index == 1 {
//            mapVc.loadData()
        }
    }
}

extension MonitoringViewController: StaffsListDelegate {
    func didSelectStaff(withId id: String) {
        mapVc.selectedStaffId = id
    }
}
