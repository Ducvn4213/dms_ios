//
//  OrderListViewCell.swift
//  DMS
//
//  Created by KID on 1/22/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

class TimeSheetViewCell: UITableViewCell {
    
    @IBOutlet weak var txtIn: UILabel!
    @IBOutlet weak var txtOut: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

