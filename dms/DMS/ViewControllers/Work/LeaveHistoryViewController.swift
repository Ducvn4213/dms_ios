import UIKit

class LeaveHistoryViewController : BaseViewController {
    
    @IBOutlet weak var tbTable: UITableView!
    
    var logData : [LeaveLog] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadData()
    }
    
    private func loadData() {
        LOADING.show()
        ApiHelperService.getLeaveLog(onSuccess: { (results) in
            LOADING.dismiss()
            self.logData = results
            self.tbTable.reloadData()
        }, onError: { (error) in
            LOADING.dismiss()
            self.showToast(message: error)
        })
    }
    
    func requestApprove(id: String) {
        LOADING.show()
        ApiHelperService.approveLeave(id: id, onSuccess: { (result) in
            LOADING.dismiss()
            self.showToast(message: result)
            self.loadData()
        }, onError: { (error) in
            LOADING.dismiss()
            self.showToast(message: error)
            self.loadData()
        })
    }
}

extension LeaveHistoryViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return logData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ts = logData[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "leave_log") as! LeaveLogViewCell
        
        cell.setData(data: ts)
        cell.parentContainer = self
        
        cell.selectionStyle = .none
        return cell
    }
}
