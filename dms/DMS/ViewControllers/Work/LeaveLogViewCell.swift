import UIKit

class LeaveLogViewCell : UITableViewCell {
    
    @IBOutlet weak var txtCreateDate: UILabel!
    @IBOutlet weak var txtFromDate: UILabel!
    @IBOutlet weak var txtToDate: UILabel!
    @IBOutlet weak var txtType: UILabel!
    @IBOutlet weak var txtStatus: UILabel!
    @IBOutlet weak var btnApprove: UIButton!
    
    var parentContainer : LeaveHistoryViewController?
    var data : LeaveLog?
    
    func setData(data: LeaveLog) {
        self.data = data
        
        txtCreateDate.text = String.init(format: "leave_log_create_date".localized(), ISOTimeToNomalTime(input: data.createDate!))
        txtFromDate.text = String.init(format: "leave_log_from_date".localized(), ISOTimeToNomalTime(input: data.fromDate!))
        txtToDate.text = String.init(format: "leave_log_to_date".localized(), ISOTimeToNomalTime(input: data.toDate!))
        txtType.text = String.init(format: "leave_log_type".localized(), data.type!)
        txtStatus.text = String.init(format: "leave_log_status".localized(), data.status!)
        
        if (data.status == "0") {
            btnApprove.isHidden = false
        }
        else {
            btnApprove.isHidden = true
        }
    }
    
    @IBAction func onApproveClicked() {
        parentContainer?.requestApprove(id: (data?.id!)!)
    }
}


