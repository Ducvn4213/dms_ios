import UIKit
import RealmSwift

class LeaveTicketsViewController : BaseViewController {
    
    @IBOutlet weak var mdvType: MyDropDownView!
    @IBOutlet weak var edtFrom: UITextField!
    @IBOutlet weak var edtTo: UITextField!
    @IBOutlet weak var mdvReason: MyDropDownView!
    
    @IBOutlet weak var tvNote: UITextView!
    
    var typeList : Results<LeaveType>?
    var reasonList : Results<LeaveReason>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
        initData()
    }
    
    private func configUI() {
        tvNote.layer.borderColor = UIColor.gray.cgColor
        tvNote.layer.borderWidth = 1
        tvNote.layer.masksToBounds = true
        
        mdvType.layer.borderColor = UIColor.gray.cgColor
        mdvType.layer.borderWidth = 1
        mdvType.layer.masksToBounds = true
        
        mdvReason.layer.borderColor = UIColor.gray.cgColor
        mdvReason.layer.borderWidth = 1
        mdvReason.layer.masksToBounds = true
    }
    
    private func initData() {
        typeList = REALM.objects(LeaveType.self)
        reasonList = REALM.objects(LeaveReason.self)
        
        var typeStringList : [String] = []
        var reasonStringList : [String] = []
        
        for t in typeList! {
            typeStringList.append(t.Name!)
        }
        
        for r in reasonList! {
            reasonStringList.append(r.Name!)
        }
        
        mdvType.dropDownView.dataSource = typeStringList
        mdvType.setDefault(index: 0)
        
        mdvReason.dropDownView.dataSource = reasonStringList
        mdvReason.setDefault(index: 0)
        
        edtFrom.text = getCurrentNormalTime()
        edtTo.text = getCurrentNormalTime()
    }
    
    @IBAction func onCancelClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitClicked() {
        let type = typeList![mdvType.dropDownView.indexForSelectedRow! + 1].Code
        let reason = reasonList![mdvReason.dropDownView.indexForSelectedRow! + 1].Code
        let from = edtFrom.text
        let to = edtTo.text
        let note = tvNote.text
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        LOADING.show()
        ApiHelperService.addLeave(deviceID: deviceID, type: type!, from: from!, to: to!, reason: reason!, note: note!, onSuccess: { (result) in
            LOADING.dismiss()
            self.showToast(message: result)
            self.navigationController?.popViewController(animated: true)
        }, onError: { (error) in
            LOADING.dismiss()
            self.showToast(message: error)
        })
    }
}
