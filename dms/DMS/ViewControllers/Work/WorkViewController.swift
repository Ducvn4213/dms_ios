import UIKit
import CoreLocation
class WorkViewController : BaseViewController {
    @IBOutlet weak var tbTable: UITableView!
    @IBOutlet weak var viewControlContainer: UIView!
    
    @IBOutlet weak var btnStartStop: UIButton!
    @IBOutlet weak var txtStartStop: UILabel!
    
    @IBOutlet weak var txtWorkingTime: UILabel!
    
    
    var timeSheets : [TimeSheetModel] = []
    
    var isWorking : Bool = false
    var startedTime : Int64 = 0
    var timer : Timer?
    
    let locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
        loadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        timer?.invalidate()
    }
    
    private func loadData() {
        LOADING.show()
        MonitorServices.getTimeSheets(onSucess: { (result) in
            LOADING.dismiss()
            
            self.timeSheets.append(contentsOf: result)
            self.tbTable.reloadData()
        }) { (error) in
            LOADING.dismiss()
            self.showMessage(message: error)
        }
        
        let lastStartedTime = getPref(key: "WORK_START_TIME")
        if (lastStartedTime != nil) {
            isWorking = true
            btnStartStop.setImage( UIImage(named: "ic_stop"), for: .normal)
            txtStartStop.text = "work_stop".localized()
            startedTime = Int64((lastStartedTime as! NSString).integerValue)
            startUpdateUITask()
        }
    
        locManager.requestWhenInUseAuthorization()
    }
    
    private func configUI() {
        viewControlContainer.layer.cornerRadius = 10
        viewControlContainer.layer.masksToBounds = true
    }
    
    @IBAction func onStartStopClicked() {
        if (isWorking) {
            confirmStopWork()
        }
        else {
            confirmStartWork()
        }
    }
    
    private func confirmStartWork() {
        let title = "dialog_title_notice".localized()
        let messa = "work_punchinmes".localized()
        let ok = "dialog_ok".localized()
        let cancel = "dialog_cancel".localized()
        
        let refreshAlert = UIAlertController(title: title, message: messa, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: ok, style: .default, handler: { (action: UIAlertAction!) in
            self.startWork()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    private func confirmStopWork() {
        let title = "dialog_title_notice".localized()
        let messa = "work_punchoutmes".localized()
        let ok = "dialog_ok".localized()
        let cancel = "dialog_cancel".localized()
        
        let refreshAlert = UIAlertController(title: title, message: messa, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: ok, style: .default, handler: { (action: UIAlertAction!) in
            self.stopWork()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: cancel, style: .cancel, handler: nil))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    
    private func startWork() {
        var currentLocation: CLLocation!
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse){
            currentLocation = locManager.location
            
            let lat = "\(currentLocation.coordinate.longitude)"
            let lon = "\(currentLocation.coordinate.latitude)"
            
            LOADING.show()
            MonitorServices.addTimeSheet(lat: lat, lon: lon,  onSucess: { (result) in
                LOADING.dismiss()
                self.btnStartStop.setImage( UIImage(named: "ic_stop"), for: .normal)
                self.txtStartStop.text = "work_stop".localized()
                self.isWorking = !self.isWorking
                
                savePref(data: result, key: "TSId")
                
                self.workStarted()
            }) { (error) in
                LOADING.dismiss()
                self.showMessage(message: error)
            }
        }
        else {
            locManager.requestWhenInUseAuthorization()
        }
    }
    
    private func stopWork() {
        var currentLocation: CLLocation!
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse){
            currentLocation = locManager.location
            
            let lat = "\(currentLocation.coordinate.longitude)"
            let lon = "\(currentLocation.coordinate.latitude)"
            let id = getPref(key: "TSId") as! String
            
            LOADING.show()
            MonitorServices.updateTimeSheet(id: id, lat: lat, lon: lon,  onSucess: { (result) in
                LOADING.dismiss()
                self.btnStartStop.setImage( UIImage(named: "ic_play"), for: .normal)
                self.txtStartStop.text = "work_start".localized()
                self.isWorking = !self.isWorking
                
                self.workStopped()
            }) { (error) in
                LOADING.dismiss()
                self.showMessage(message: error)
            }
        }
        else {
            locManager.requestWhenInUseAuthorization()
        }
    }
    
    private func workStarted() {
        startedTime = Int64(NSDate().timeIntervalSince1970)
        savePref(data: String(startedTime), key: "WORK_START_TIME")
        
        startUpdateUITask()
        showMessage(message: "work_punchinsuccess".localized())
    }
    
    private func workStopped() {
        removePref(key: "WORK_START_TIME")
        
        stopUpdateUITask()
        let title = "dialog_title_notice".localized()
        let messa = "work_punchoutsuccess".localized()
        let ok = "dialog_ok".localized()
        
        let refreshAlert = UIAlertController(title: title, message: messa, preferredStyle: UIAlertControllerStyle.alert)
        
        refreshAlert.addAction(UIAlertAction(title: ok, style: .default, handler: { (action: UIAlertAction!) in
            LOADING.show()
            MonitorServices.getTimeSheets(onSucess: { (result) in
                LOADING.dismiss()
    
                self.timeSheets.append(contentsOf: result)
                self.tbTable.reloadData()
            }) { (error) in
                LOADING.dismiss()
                self.showMessage(message: error)
            }
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    private func startUpdateUITask() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(WorkViewController.updateTimer)), userInfo: nil, repeats: true)
    }
    
    private func stopUpdateUITask() {
        timer?.invalidate()
        
        txtWorkingTime.text = "00:00:00"
    }
    
    func updateTimer() {
        let current = Int64(NSDate().timeIntervalSince1970)
        let seconds = current - startedTime
        
        txtWorkingTime.text = secondsToTime(input: seconds)
    }
}

extension WorkViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return timeSheets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let ts = timeSheets[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "timesheetcell") as! TimeSheetViewCell
        cell.txtIn.text = ISOTimeToNomalTime(input: ts.startTime)
        cell.txtOut.text = ISOTimeToNomalTime(input: ts.endTime)
    
        cell.selectionStyle = .none
        return cell
    }
}
