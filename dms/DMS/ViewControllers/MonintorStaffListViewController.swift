//
//  MonintorStaffListViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class MonintorStaffListViewController: UIViewController, MapCellDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    weak var delegate: StaffsListDelegate?
    var staffs: [StaffModel] = []
    var sLocations: [StaffLocationModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 175
        
//        SpinnerHelper.sharedInstance.startAnimatingInView(containingView: self.view)
        
//        fakeData()
//        loadData()
        

    }
    
    func loadData() {
        
        self.staffs = APPDELEGATE.staffs
        self.sLocations = APPDELEGATE.sLocations
        tableView.reloadData()
        
//        let date = Date().toStringMMDDYYYY()
//        MonitorServices.getStaffs(updateDate: date, onSucess: { (staffs) in
//            self.staffs = staffs
//            self.tableView.reloadData()
//            SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
//            self.loadstatus += 1
//        }) { (error) in
//            SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
//            self.showMessage(message: error)
//        }
    }
    func fakeData() {
        let staff1 : StaffModel = StaffModel(id: "1", empId: "1", name: "Nguyễn Văn A", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", date: "10/10/2017", checkInPlan: "11", checkInFinish: "5", noOrder: "0", lastCustomerName: "1", lastCustomerAddress: "2 Nguyễn Thế Lộc", noofMetter: "1000")
        let staff2 : StaffModel = StaffModel(id: "2", empId: "2", name: "Nguyễn Văn B", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", date: "9/10/2017", checkInPlan: "7", checkInFinish: "4", noOrder: "0", lastCustomerName: "2", lastCustomerAddress: "3 Nguyễn Thế Lộc", noofMetter: "2000")
        staffs.append(staff1)
        staffs.append(staff2)
        self.tableView.reloadData()
        SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
    }
    
    func showMessage(message: String) {
        let alertController = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alertController.addAction(action)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showMap(staff: StaffLocationModel) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Monitor", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "mapview") as! MapViewController
        
        vc.staff = staff
        vc.title = staff.name
        
        self.parent?.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MonintorStaffListViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (staffs.count == sLocations.count) {
            return staffs.count
        } else {
            return 0
        }
//        return staffs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffTableViewCell", for: indexPath) as! StaffTableViewCell
        let staff = staffs[indexPath.row]
        cell.configure(withStaff: staff)
        cell.totalLabel.text = staff.noOrder
        cell.planLabel.text = staff.checkInPlan
        cell.finshLabel.text = staff.checkInFinish
        cell.locationStaff = sLocations[indexPath.row]
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let staff = staffs[indexPath.row]
        delegate?.didSelectStaff(withId: staff.empId)
    }
}

protocol StaffsListDelegate: class {
    func didSelectStaff(withId id:String)
}
