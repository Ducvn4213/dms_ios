//
//  MapViewController.swift
//  DMS
//
//  Created by KID on 10/17/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import GoogleMaps
import PopupDialog

class MapViewController: BaseViewController, GMSMapViewDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    var staff: StaffLocationModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        if staff != nil {
            self.title = staff.name
            let location2D = CLLocationCoordinate2D(latitude: staff.latitude, longitude: staff.longitude)
            
            let marker = GMSMarker(position: location2D)
            
            marker.title = staff.name
            marker.snippet = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", staff.checkinPlan, staff.checkinFinished, staff.order)
            marker.map = self.mapView
            
            self.mapView.animate(to: GMSCameraPosition.camera(withTarget: location2D, zoom: 12))
        }
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: "login")
        
        let cview = UIView(frame: CGRect(x: 0, y: 0, width: 100, height: 100), reuseIdentifier: "dsd")
        cview.backgroundColor = UIColor.red
        mapView.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        let vs = WindowInfoViewController(nibName: "WindowInfoViewController", bundle: nil)
        
        vs.staff = staff
        
        
        let p = PopupDialog(viewController: vs)
        //        let button = DefaultButton(title: "OK", action: nil)
        //        p.addButton(button)
        
        self.present(p, animated: true, completion: nil)
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
