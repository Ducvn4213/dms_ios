//
//  LoginViewController.swift
//  DMS
//
//  Created by KID on 10/6/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import Toast_Swift
import Localize_Swift
import FontAwesome_swift

class LoginViewController: BaseViewController {
    
    @IBOutlet weak var txtUser: UITextField!
    @IBOutlet weak var txtPass: UITextField!
    @IBOutlet weak var labelLanguage: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var imgLanguage: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        imgLanguage.image = UIImage.fontAwesomeIcon(name: .chevronDown, textColor: UIColor.black, size: CGSize(width: 24, height: 24))
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.autoLogin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func autoLogin() {
        let user = getPref(key: autologinkey)
        let SalesManId = getPref(key: sale_id_key)
        let WorkingDate = getPref(key: working_date)
        
        if ((user) != nil) {
            APPDELEGATE.userCode = user as! String
            APPDELEGATE.SalesManId = SalesManId as! String
            APPDELEGATE.WorkingDate = WorkingDate as! String
            self.changeLanguage()
            self.gotoMain()
        }
    }
    
    func validate() -> Bool {
        if (txtUser.text == "") {
            view.makeToast("User name is required".localized())
            return false
        }
        
        if (txtPass.text == "") {
            view.makeToast("Password is required".localized())
            return false
        }
        
        
        if ((txtPass.text?.count)! < min_pass_count) {
            view.makeToast("Password too short".localized())
            //            txtNewPass.becomeFirstResponder()
            return false
        }
        return true
    }
    
    @IBAction func actionLogin(_ sender: Any) {
        
        if (validate()) {
            LOADING.show()
            
            let user = txtUser.text
            let pass = txtPass.text
            LoginServices.login(UserCode : user!, Password : pass!, onSucess: { (result, userCode, SalesManId, WorkingDate) in
                
                if result {
                    savePref(data: user!, key: autologinkey)
                    savePref(data: SalesManId, key: sale_id_key)
                    savePref(data: WorkingDate, key: working_date)
                    APPDELEGATE.userCode = user
                    APPDELEGATE.SalesManId = SalesManId
                    APPDELEGATE.WorkingDate = WorkingDate
                    SyncService.sharedInstance.downloadData(userName: user as! String, onSucess: {
                        message in
                        LOADING.dismiss()
                        self.gotoMain()
                    }, onError: {error in
                        LOADING.dismiss()
                        self.showMessage(message: "Can't sync data, please try again")
                    })
                } else {
                    LOADING.dismiss()
                    self.view.makeToast("Something error".localized())
                }
                
                print("Result \(result), UserCode \(userCode)")
            }) { (error) in
                self.showMessage(message: error)
                LOADING.dismiss()
            }
        }
    }
    
    func gotoMain() {
        if let storyboard = self.storyboard {
            let nv = storyboard.instantiateViewController(withIdentifier: "Navi") as! UINavigationController
            
            self.present(nv, animated: false, completion: nil)
            
            nv.view.makeToast("Login successfully".localized())
        }
    }
    
    @IBAction func actionLanguage(_ sender: Any) {
        let alert = UIAlertController(title: "Language".localized(), message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "English".localized(), style: .default, handler: { (action) in
            Localize.setCurrentLanguage(english)
            BundleLocalization.sharedInstance().language = "en"
            savePref(data: "en", key: "APP_LANGUAGE")
            self.setText()
        }))
        
        alert.addAction(UIAlertAction(title: "Vietnamese".localized(), style: .default, handler: { (action) in
            Localize.setCurrentLanguage(vietnam)
            BundleLocalization.sharedInstance().language = "vi"
            savePref(data: "vi", key: "APP_LANGUAGE")
            self.setText()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func changeLanguage() {
        var lang = getPref(key: "APP_LANGUAGE") as? String
        if (lang == nil) {
            lang = "en"
        }
        
        Localize.setCurrentLanguage(lang!)
        BundleLocalization.sharedInstance().language = lang
        self.setText()
    }
    
    
    override func setText() {
        super.setText()
        labelLanguage.text = getDetailLanguage(language: Localize.currentLanguage())
        btnLogin.setTitle("LOGIN".localized(), for: .normal)
        labelTitle.text = "Login Screen".localized()
        txtUser.placeholder = "Username".localized()
        txtPass.placeholder = "Password".localized()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
