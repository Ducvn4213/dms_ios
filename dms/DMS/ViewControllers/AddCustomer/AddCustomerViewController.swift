//
//  AddCustomerViewController.swift
//  DMS
//
//  Created by KID on 12/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import GooglePlacePicker

class AddCustomerViewController: BaseViewController, GMSPlacePickerViewControllerDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var txtAddress: UITextField!
    @IBOutlet weak var dropDownType: MyDropDownView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtPersonal: UITextField!
    @IBOutlet weak var txtNote: UITextField!
    @IBOutlet weak var txtPhone: UITextField!
    
    var result: ((_ result: Bool) -> ())!
    
    var address: CLLocationCoordinate2D!
    @IBOutlet weak var mapView: GMSMapView!
    
    var marker: GMSMarker!
    let locManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        var groups = [String]()
        
        for group in REALM.objects(CustomerGroup.self) {
            groups.append(group.GrpName!)
        }
        
        dropDownType.dropDownView.dataSource = groups
        dropDownType.setDefault()
        
        locManager.requestWhenInUseAuthorization()
        getCurrentLocation()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.authorizedAlways ||
            status == CLAuthorizationStatus.authorizedWhenInUse) {
            getCurrentLocation()
        }
        else {
            //TODO
        }
    }
    
    private func getCurrentLocation() {
        var currentLocation: CLLocation!
        if( CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedAlways ||
            CLLocationManager.authorizationStatus() == CLAuthorizationStatus.authorizedWhenInUse){
            currentLocation = locManager.location
            
            marker = GMSMarker(position: currentLocation.coordinate)
            marker.map = mapView
            
            mapView.camera = GMSCameraPosition(target: currentLocation.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
        else {
            locManager.requestWhenInUseAuthorization()
        }
    }
    
    @IBAction func actionPlacePicker(_ sender: Any) {
        
        let config = GMSPlacePickerConfig(viewport: nil)
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        
        present(placePicker, animated: true, completion: nil)
    }
    
    // To receive the results from the place picker 'self' will need to conform to
    // GMSPlacePickerViewControllerDelegate and implement this code.
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        let pos = place.coordinate
        
        txtAddress.text = place.formattedAddress
        
        marker = GMSMarker(position: pos)
        marker.map = mapView
        
        mapView.camera = GMSCameraPosition(target: pos, zoom: 15, bearing: 0, viewingAngle: 0)
        
//        print("Place name \(place.name)")
//        print("Place address \(place.formattedAddress)")
//        print("Place attributions \(place.attributions)")
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
    @IBAction func actionOk(_ sender: Any) {
        
        let group = REALM.objects(CustomerGroup.self)[dropDownType.dropDownView.indexForSelectedRow!]
        
        if (validate()) {
            CustomerServices.addCustomer(CardName: txtName.text!, ContactPerson: txtPersonal.text!, Tel: txtPhone.text!, LatitudeValue: String(address.latitude), LongitudeValue: String(address.longitude), CardGroup: group.GrpCode!, Reamarks: txtNote.text!, Address: txtAddress.text!, onSucess: {(message) in
//                self.showDialog(title: "iDMS Management", message: message)
//
                self.showDialog(title: "iDMS Management", message: message, handlerOK: {(UIAlertAction) in
                    self.result(true)
                    self.navigationController?.popViewController(animated: true)
                })
            }, onError: {(error) in
                self.showDialog(title: "iDMS Management", message: error)
            })
        }
    }
    
    func validate() -> Bool {
        if (txtName.text == "") {
            view.makeToast("Missing card name".localized())
            return false
        }
        
        if (txtPersonal.text == "") {
            view.makeToast("Missing contact person name".localized())
            return false
        }
        
        
        if (txtPhone.text == "") {
            view.makeToast("Missing phone number".localized())
            return false
        }
        
        if (txtAddress.text == "") {
            view.makeToast("Missing address".localized())
            return false
        }
        return true
    }

}
