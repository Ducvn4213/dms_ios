//
//  BookingViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 7/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class BookingViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    
    let productCellId = "ProductTableViewCell"
    let headerCellId = "ProductHeaderTableViewCell"
    var products: [ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: productCellId)
        tableView.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: headerCellId)
    }
}

extension BookingViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: headerCellId, for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: productCellId, for: indexPath) as! ProductTableViewCell
            let product = products[indexPath.row]
            return cell
        }
    }
}
