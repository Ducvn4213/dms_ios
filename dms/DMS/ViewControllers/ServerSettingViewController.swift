import UIKit

class ServerSettingViewController : BaseViewController {
    @IBOutlet weak var txtURL: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData();
    }
    
    private func initData() {
        txtURL.text = getCurrentApiUrl()
    }
    
    @IBAction func onChangeClicked(_ sender: Any) {
        setNewApiUrl(data: txtURL.text!)
        self.dismiss(animated: true, completion: nil)
    }
}
