//
//  MonitorMapViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import MapKit
import GoogleMaps
import PopupDialog

class MonitorMapViewController: BaseViewController, GMSMapViewDelegate {
    //    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var gMapView: GMSMapView!
    
    //    let AnnotationId = "NMAnnotationId"
    
    var selectedStaffId = ""
    
    var visitPlaces: [VisitingModel] = []
    
    var staffPositions: [StaffLocationModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gMapView.delegate = self
        //        loadData()
        //        fakeData()
        //        mapView.delegate = self
    }
    
    func fakeData() {
        let staffLocation1 = StaffLocationModel(id: "1", name: "Nguyễn Văn A", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", latitude: 10.818084, longitude: 106.630507, order: "0", checkinPlan: "11", checkinFinished: "5")
        let staffLocation2 = StaffLocationModel(id: "2", name: "Nguyễn Văn B", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", latitude: 10.809162473387458, longitude: 106.64850220219253, order: "0", checkinPlan: "7", checkinFinished: "4")
        
        staffPositions.append(staffLocation1)
        staffPositions.append(staffLocation2)
        
        let visit1 = VisitingModel.init(id: "1", latitude: 10.818084, longitude: 106.630507, order: 1, name: "Nguyễn Văn A", address: "2 Nguyễn Thế Lộc", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", empName: "Nguyễn Văn B", checkInPlan: "7", checkInFinish: "4", noOrder: "0")
        
        let visit2 = VisitingModel.init(id: "2", latitude: 10.82323, longitude: 106.640507, order: 1, name: "Nguyễn Văn B", address: "2 Nguyễn Thế Lộc", avatarUrl: "https://i.pinimg.com/564x/7c/c7/a6/7cc7a630624d20f7797cb4c8e93c09c1.jpg", empName: "Nguyễn Văn B", checkInPlan: "7", checkInFinish: "4", noOrder: "0")
        
        visitPlaces.append(visit1)
        visitPlaces.append(visit2)
        
        if selectedStaffId == "" {
            SpinnerHelper.sharedInstance.startAnimatingInView(containingView: self.view)
            self.drawCurrentPositionsOfStaffOnGMap(staffPositions: staffPositions)
            SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
        } else {
            self.drawItemsOnGMap()
            //            self.drawPath()
        }
    }
    
    func loadData() {
        self.drawCurrentPositionsOfStaffOnGMap(staffPositions: APPDELEGATE.sLocations)
        //        if selectedStaffId == "" {
        //            SpinnerHelper.sharedInstance.startAnimatingInView(containingView: self.view)
        //            MonitorServices.getAllStaffsLocation(onSucess: { (staffPositions) in
        //                SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
        //                self.drawCurrentPositionsOfStaffOnGMap(staffPositions: staffPositions)
        //            }, onError: { (error) in
        //                SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
        //                self.showMessage(message: error)
        //            })
        //        } else {
        //            SpinnerHelper.sharedInstance.startAnimatingInView(containingView: self.view)
        //            MonitorServices.getAllVisitedCustomers(forStaffId: selectedStaffId, updateDate: Date().toStringMMDDYYYY(), onSucess: { (visitedPlaces) in
        //                SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
        //                self.visitPlaces = visitedPlaces
        //                self.drawItemsOnGMap()
        ////                self.drawPath()
        //            }) { (error) in
        //                SpinnerHelper.sharedInstance.stopAnimatingInView(containingView: self.view)
        //                self.showMessage(message: error)
        //            }
        //        }
    }
    
    //    func drawItemsOnMap() {
    //        mapView.removeAnnotations(mapView.annotations)
    //        var zoomRect: MKMapRect = MKMapRectNull
    //        for place in visitPlaces {
    //            let location2D = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
    //            let annotation = CustomAnnotation(orderNumber: place.order, avatarUrl: place.avatarUrl, coordinate: location2D)
    //            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
    //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 100, 100)
    //            zoomRect = MKMapRectUnion(zoomRect, pointRect)
    //
    //            annotation.title = place.empName + " - " + place.name
    //            annotation.subtitle = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", place.checkInPlan, place.checkInFinish, place.noOrder)
    //            mapView.addAnnotation(annotation)
    //        }
    //
    //        if visitPlaces.count > 0 {
    //            let inset = -zoomRect.size.width * 0.15
    //            mapView.setVisibleMapRect(MKMapRectInset(zoomRect, inset, inset), animated: true)
    //        }
    //    }
    
    func drawItemsOnGMap() {
        gMapView.clear()
        
        var bounds = GMSCoordinateBounds()
        
        
        for (index, place) in visitPlaces.enumerated() {
            
            
            let location2D = CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude)
            
            let marker = GMSMarker(position: location2D)
            
            marker.title = place.empName + " - " + place.name
            marker.snippet = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", place.checkInPlan, place.checkInFinish, place.noOrder)
            marker.map = self.gMapView
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        gMapView.animate(with: update)
        
        if (visitPlaces.count <= 1) {
            gMapView.animate(toZoom: 13)
        }
        
        
        
        //            let annotation = CustomAnnotation(orderNumber: place.order, avatarUrl: place.avatarUrl, coordinate: location2D)
        //            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
        //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 100, 100)
        //            zoomRect = MKMapRectUnion(zoomRect, pointRect)
        
        //            annotation.title = place.empName + " - " + place.name
        //            annotation.subtitle = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", place.checkInPlan, place.checkInFinish, place.noOrder)
        //            mapView.addAnnotation(annotation)
        //        }
        
        //        if visitPlaces.count > 0 {
        //            let inset = -zoomRect.size.width * 0.15
        //            mapView.setVisibleMapRect(MKMapRectInset(zoomRect, inset, inset), animated: true)
        //        }
    }
    
    //    func drawCurrentPositionsOfStaffOnMap(staffPositions: [StaffLocationModel]) {
    //        mapView.removeAnnotations(mapView.annotations)
    //        var zoomRect: MKMapRect = MKMapRectNull
    //        for staff in staffPositions {
    //            let location2D = CLLocationCoordinate2D(latitude: staff.latitude, longitude: staff.longitude)
    //            let annotation = CustomAnnotation(orderNumber: 0, avatarUrl: staff.avatarUrl, coordinate: location2D)
    //            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
    //            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1)
    //            zoomRect = MKMapRectUnion(zoomRect, pointRect)
    //
    //            annotation.title = staff.name
    //            annotation.subtitle = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", staff.checkinPlan, staff.checkinFinished, staff.order)
    //
    //            mapView.addAnnotation(annotation)
    //        }
    //
    //        if staffPositions.count > 0 {
    //            let inset = -zoomRect.size.width * 0.15
    //            mapView.setVisibleMapRect(MKMapRectInset(zoomRect, inset, inset), animated: true)
    //        }
    //    }
    //
    
    func drawCurrentPositionsOfStaffOnGMap(staffPositions: [StaffLocationModel]) {
        
        gMapView.clear()
        
        var bounds = GMSCoordinateBounds()
        
        for (index, staff) in staffPositions.enumerated() {
            
            let location2D = CLLocationCoordinate2D(latitude: staff.latitude, longitude: staff.longitude)
            
            let marker = GMSMarker(position: location2D)
            
            marker.title = staff.name
            marker.snippet = String(format: "Kế hoạch: %@ - Đã thực hiện: %@ - Tổng đơn hàng: %@", staff.checkinPlan, staff.checkinFinished, staff.order)
            marker.userData = index
            
            marker.map = self.gMapView
            bounds = bounds.includingCoordinate(marker.position)
        }
        
        let update = GMSCameraUpdate.fit(bounds, withPadding: 50)
        gMapView.animate(with: update)
        
        if (staffPositions.count <= 1) {
            gMapView.animate(toZoom: 12)
        }
        
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        let vs = WindowInfoViewController(nibName: "WindowInfoViewController", bundle: nil)
        
        let index = marker.userData as! Int
        
        
        vs.staff = APPDELEGATE.sLocations[index]
        
        
        let p = PopupDialog(viewController: vs)
        //        let button = DefaultButton(title: "OK", action: nil)
        //        p.addButton(button)
        
        self.present(p, animated: true, completion: nil)
        return true
    }
    
    
    //    func drawPath() {
    //        mapView.removeOverlays(mapView.overlays)
    //        for i in 0..<visitPlaces.count {
    //            if i < visitPlaces.count - 1 {
    //                let place = visitPlaces[i]
    //                let nextPlace = visitPlaces[i+1]
    //
    //                let request = MKDirectionsRequest()
    //                request.source = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: place.latitude, longitude: place.longitude), addressDictionary: nil))
    //                request.destination = MKMapItem(placemark: MKPlacemark(coordinate: CLLocationCoordinate2D(latitude: nextPlace.latitude, longitude: nextPlace.longitude), addressDictionary: nil))
    //                request.requestsAlternateRoutes = false
    //
    //                request.transportType = .any
    //
    //                let directions = MKDirections(request: request)
    //                directions.calculate { [weak self] (response, error) in
    //                    if error != nil {
    ////                        DDLogError("[DIRECTION] Failed to calculate direction to shop. Error: \(error!)")
    //                        return
    //                    }
    //
    //                    guard let response = response else {
    ////                        DDLogError("[DIRECTION] Failed to calculate direction to shop.")
    //                        return
    //                    }
    //
    //                    guard let strongSelf = self else { return }
    //
    //                    if response.routes.count > 0 {
    //                        let route = response.routes[0]
    //                        strongSelf.mapView.add(route.polyline, level: .aboveRoads)
    //                    }
    //                }
    //            }
    //        }
    //
    //    }
}

//extension MonitorMapViewController : MKMapViewDelegate {
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: AnnotationId)
//        if annotationView == nil {
//            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: AnnotationId)
//        } else {
//            annotationView?.annotation = annotation
//        }
//
//
//        if annotation is MKUserLocation {
//        } else if annotation is CustomAnnotation {
//            annotationView = CustomAnnotationView(annotation: annotation)
//        }
//
//        annotationView?.canShowCallout = true
////        annotationView?.calloutOffset = CGPoint(x: -20, y: -20)
////        annotationView?.centerOffset = CGPoint(x: 20, y: 20)
//
//        return annotationView
//    }
//
//    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
//        if overlay is MKPolyline {
//            let renderer = MKPolylineRenderer(polyline: overlay as! MKPolyline)
//            renderer.strokeColor = UIColor.random()
//            renderer.lineWidth = 3
//            return renderer
//        }
//
//        return MKOverlayRenderer()
//    }
//
//    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
//        if let annotation = view.annotation {
////            mapView.selectAnnotation(annotation, animated: true)
//        }
//    }
//
//}
//
//class CustomAnnotation: MKPointAnnotation {
//    var orderNumber: Int = 0
//    var avatarUrl = ""
//
//    init(orderNumber: Int, avatarUrl: String, coordinate: CLLocationCoordinate2D) {
//        super.init()
//        self.orderNumber = orderNumber
//        self.avatarUrl = avatarUrl
//
//        self.coordinate = coordinate
//    }
//}
//
//
//class CustomAnnotationView: MKAnnotationView {
//    init(annotation: MKAnnotation) {
//        super.init(annotation: annotation, reuseIdentifier: nil)
//
//        if let annotation = annotation as? CustomAnnotation {
//            self.image = UIImage(named: "pinWithNumber")
//
//            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
//            imageView.sd_setImage(with: URL(string: annotation.avatarUrl))
//            self.leftCalloutAccessoryView = imageView
//
//            if annotation.orderNumber > 0 {
//                var rect = CGRect(x: 8, y: 4, width: 15, height: 15)
//                if ScreenSize.CurrentType == ScreenType.ip6 {
//                    rect = CGRect(x: 10, y: 5, width: 17, height: 17)
//                } else if ScreenSize.CurrentType == ScreenType.ip6Plus {
//                    rect = CGRect(x: 12, y: 6, width: 20, height: 20)
//                }
//
//                let numberLabel = UILabel(frame: rect)
//                numberLabel.textAlignment = .center
//                numberLabel.text = String(annotation.orderNumber)
//                numberLabel.textColor = UIColor.blue
//                self.addSubview(numberLabel)
//            }
//
//        }
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
//        let hitView = super.hitTest(point, with: event)
//        if hitView != nil {
//            self.superview?.bringSubview(toFront: self)
//        }
//        return hitView
//    }
//    
//    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
//        let rect = self.bounds
//        var isInside = rect.contains(point)
//        
//        if (!isInside) {
//            for view in self.subviews {
//                isInside = view.frame.contains(point)
//                if isInside {
//                    break
//                }
//            }
//        }
//        return isInside
//    }
//}

