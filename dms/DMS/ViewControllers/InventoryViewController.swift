//
//  InventoryViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 7/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class InventoryViewController: UIViewController {
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var btnCart: UIButton!
    @IBOutlet weak var btnQR: UIButton!
    
    let productCellId = "ProductTableViewCell"
    let headerCellId = "ProductHeaderTableViewCell"
    var delegate: InventoryDelegate?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(UINib(nibName: "ProductTableViewCell", bundle: nil), forCellReuseIdentifier: productCellId)
        tableView.register(UINib(nibName: "ProductHeaderTableViewCell", bundle: nil), forCellReuseIdentifier: headerCellId)
        btnCart.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
        btnCart.setTitle(String.fontAwesomeIcon(name: .cartPlus), for: .normal)
        btnQR.titleLabel?.font = UIFont.fontAwesome(ofSize: 17)
        btnQR.setTitle(String.fontAwesomeIcon(name: .qrcode) + " Quét mã QR".localized(), for: .normal)
    }
    
    @IBAction func addRow() {
//        let emptyProduct = ProductModel(itemId: "", barcode: "", name: "")
//        products.append(emptyProduct)
//        self.tableView.reloadData()
//        self.delegate?.didAddedRow()
        let vc = UIStoryboard.productSearchViewController()
        vc.products = REALM.objects(Product.self).toArray(ofType: Product.self)
        vc.delegate = self
        
        let parent = self.delegate as! CustomerDetailViewController
        parent.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func actionSave(_ sender: Any) {
        delegate?.didSave()
    }
    
    @IBAction func actionQR(_ sender: Any) {
        delegate?.didBarcode()
    }
    
    
}

extension InventoryViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return APPDELEGATE.stocks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: headerCellId, for: indexPath)
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: productCellId, for: indexPath) as! ProductTableViewCell
            cell.index = indexPath.row
            cell.configure()
            return cell
        }
    }
}

extension InventoryViewController: ProductSearchDelegate {
    func selected(product: Product) {
        let parent = self.delegate as! CustomerDetailViewController
        for item in APPDELEGATE.stocks {
            if(item.ItemCode == product.ItemCode) {
                
                let alertController = UIAlertController(title: "Có lỗi".localized(), message: "Không thể chọn sản phẩm đã tồn tại".localized(), preferredStyle: .alert)
                
                let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: nil)
                alertController.addAction(ok)
                
                parent.navigationController?.present(alertController, animated: true, completion: nil)
                return
            }
        }
        
        let alertController = UIAlertController(title: "Thêm sản phẩm".localized(), message: "Bạn có chắc chắn muốn thêm sản phẩm này không".localized(), preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK".localized(), style: .default, handler: {(UIAlertAction) in
            APPDELEGATE.stocks.append(product)
            self.tableView.reloadData()
            self.delegate?.didAddedRow()
            parent.navigationController?.popViewController(animated: true)
        })
        let cancel = UIAlertAction(title: "Huỷ".localized(), style: .cancel, handler: nil)
        alertController.addAction(ok)
        alertController.addAction(cancel)
        parent.navigationController?.present(alertController, animated: true, completion: nil)
        
        
    }
    
    
}

protocol InventoryDelegate {
    func didAddedRow()
    func didSave()
    func didBarcode()
}
