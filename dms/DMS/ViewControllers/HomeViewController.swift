//
//  ViewController.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {

    @IBOutlet weak var txtName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initData();
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func initData() {
        let welcomeStringFormat = "home_hello".localized()
        let welcomeString = String.init(format: welcomeStringFormat, arguments: [APPDELEGATE.userCode])
        txtName.text = welcomeString
    }
    
    @IBAction func visit() {
        
    }
    
    @IBAction func orders() {
//        self.showMessage(message: notImplementMessage)
    }
    
    @IBAction func monitoring() {
        self.showMessage(message: notImplementMessage)
    }
    
    @IBAction func sync() {
        LOADING.show()
        let listObj = REALM.objects(SyncObject.self).toArray(ofType: SyncObject.self)
        SyncService.sharedInstance.processSyncWith(syncObjects: listObj, onSucess: {(string) in
            SyncService.sharedInstance.downloadData(userName: APPDELEGATE.userCode, onSucess: { (message) in
                print(message)
                LOADING.dismiss()
                self.showDialog(title: "Sync", message: "Đồng bộ thành công!")
            }, onError: { (error) in
                LOADING.dismiss()
                self.showToast(message: string)
                self.showDialog(title: "Error", message: "Đồng bộ thất bại".localized())
            })
        }, onError: {(string) in
            LOADING.dismiss()
            self.showToast(message: string)
            self.showDialog(title: "Error", message: "Đồng bộ thất bại".localized())
        })
    }
    
    @IBAction func actionLogout(_ sender: Any) {
        removePref(key: autologinkey)
        APPDELEGATE.userCode = nil
        gotoViewController(name: "login")
    }
    
}

