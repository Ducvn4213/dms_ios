//
//  OrderServices.swift
//  DMS
//
//  Created by KID on 1/7/18.
//  Copyright © 2018 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash
import MagicalRecord


class OrderServices {
    static func addOrder(UserCode: String, Cardcode: String, OrderDate: String, DeliveryDate: String, CardName: String, ItemCode: String, ItemName: String, ItemType: String, Quantity: String, Uom: String, Price: String, VAT: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + customer_add_order_url
        
        let pagram: Parameters = ["Cardcode" : Cardcode, "UserCode": UserCode, "OrderDate": OrderDate, "DeliveryDate": DeliveryDate, "CardName": CardName, "ItemCode": ItemCode, "ItemName": ItemName, "ItemType": ItemType, "Quantity": Quantity, "Uom": Uom, "Price": Price, "VAT": VAT]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultID = customerXml["ResultID"].element?.text ?? ""
                    onSucess(ResultID)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getOrderByDocEntry(DocEntry: String, onSucess: @escaping ([OrderItem]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_order_by_docentry
        let pagram: Parameters = ["DocEntry": DocEntry]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[OrderItem] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let orderXml = table[i]                        
                        let MyId = orderXml["MyId"].element?.text ?? ""
                        let DocEntry = orderXml["DocEntry"].element?.text ?? ""
                        let ItemCode = orderXml["ItemCode"].element?.text ?? ""
                        let BarCode = orderXml["BarCode"].element?.text ?? ""
                        let ItemName = orderXml["ItemName"].element?.text ?? ""
                        let Quantity = orderXml["Quantity"].element?.text ?? ""
                        let UoM = orderXml["UoM"].element?.text ?? ""
                        let Price = orderXml["Price"].element?.text ?? ""
                        let Discount = orderXml["Discount"].element?.text ?? ""
                        let PriceAfterDiscount = orderXml["PriceAfterDiscount"].element?.text ?? ""
                        let VATAmt = orderXml["VATAmt"].element?.text ?? ""
                        let VATPercent = orderXml["VATPercent"].element?.text ?? ""
                        let GTotal = orderXml["GTotal"].element?.text ?? ""
                        let DocEntry1 = orderXml["DocEntry1"].element?.text ?? ""
                        let CardCode = orderXml["CardCode"].element?.text ?? ""
                        let CardName = orderXml["CardName"].element?.text ?? ""
                        let Address = orderXml["Address"].element?.text ?? ""
                        let Route = orderXml["Route"].element?.text ?? ""
                        let OrderType = orderXml["OrderType"].element?.text ?? ""
                        let OrderTypeName = orderXml["OrderTypeName"].element?.text ?? ""
                        let PONo = orderXml["PONo"].element?.text ?? ""
                        let PODate = orderXml["PODate"].element?.text ?? ""
                        let DeliveryNo = orderXml["DeliveryNo"].element?.text ?? ""
                        let DeliveryDate = orderXml["DeliveryDate"].element?.text ?? ""
                        let DocStatus = orderXml["DocStatus"].element?.text ?? ""
                        let DocStatusName = orderXml["DocStatusName"].element?.text ?? ""
                        let SalesEmp = orderXml["SalesEmp"].element?.text ?? ""
                        let DeliveryEmp = orderXml["DeliveryEmp"].element?.text ?? ""
                        let DocTotal = orderXml["DocTotal"].element?.text ?? ""
                        let DocVATTotal = orderXml["DocVATTotal"].element?.text ?? ""
                        let DocGToal = orderXml["DocGToal"].element?.text ?? ""
                        let Remark = orderXml["Remark"].element?.text ?? ""
                        let CreateDate = orderXml["CreateDate"].element?.text ?? ""
                        
                        let order = OrderItem(MyId: MyId, DocEntry: DocEntry, ItemCode: ItemCode, BarCode: BarCode, ItemName: ItemName, Quantity: Quantity, UoM: UoM, Price: Price, Discount: Discount, PriceAfterDiscount: PriceAfterDiscount, VATAmt: VATAmt, VATPercent: VATPercent, GTotal: GTotal, DocEntry1: DocEntry1, CardCode: CardCode, CardName: CardName, Address: Address, Route: Route, OrderType: OrderType, OrderTypeName: OrderTypeName, PONo: PONo, PODate: PODate, DeliveryNo: DeliveryNo, DeliveryDate: DeliveryDate, DocStatus: DocStatus, DocStatusName: DocStatusName, SalesEmp: SalesEmp, DeliveryEmp: DeliveryEmp, DocTotal: DocTotal, DocVATTotal: DocVATTotal, DocGToal: DocGToal, Remark: Remark, CreateDate: CreateDate)
                        results.append(order)
                    }
                }
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func cancelOrder(UserCode: String, DocEntry: String, onSucess: @escaping (String, String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + order_updatedelivery_cancled
        
        let pagram: Parameters = ["DocEntry" : DocEntry, "UserCode": UserCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let result = customerXml["Rsult"].element?.text ?? ""
                    let msg = customerXml["ResultString"].element?.text ?? ""
                    onSucess(result, msg)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func updateDeliveryOrder(UserCode: String, DocEntry: String, DeliveryDate_Act: String, NotesDelivery: String, ItemCode: String, Quantity: String, Quantity_Del: String, onSucess: @escaping (String, String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + order_updatedelivery_byitem
        
        let pagram: Parameters = ["DocEntry" : DocEntry, "UserCode": UserCode, "DeliveryDate_Act": DeliveryDate_Act, "NotesDelivery": NotesDelivery, "ItemCode": ItemCode, "Quantity": Quantity, "Quantity_Del": Quantity_Del]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let result = customerXml["Rsult"].element?.text ?? ""
                    let msg = customerXml["ResultString"].element?.text ?? ""
                    onSucess(result, msg)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func updateOrder(userCode: String, docEntry: String, date: String, status: String, note: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + order_updatedelivery_update
        
        let pagram: Parameters = [
            "DocEntry" : docEntry,
            "DeliveryDate_Act": date,
            "DocStatus": status,
            "NotesDelivery": note,
            "UserCode": userCode
        ]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let result = customerXml["ResultString"].element?.text ?? ""
                    
                    if (result.contains("UnSuccessfully")) {
                        onError(result)
                        return
                    }
                    
                    onSucess(result)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
}
