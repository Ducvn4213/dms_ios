//
//  CustomerServices.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash
import MagicalRecord

class CustomerServices {
    static func getCustomers(onSucess: @escaping ([Customer]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/IDN_DMS_OCRD_Load?UserCode=" + APPDELEGATE.userCode
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Customer] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["CardCode"].element?.text ?? ""
                    let name = customerXml["CardName"].element?.text ?? ""
                    let address = customerXml["Address"].element?.text ?? ""
                    let contact = customerXml["ContactPerson"].element?.text ?? ""
                    let tel = customerXml["Tel"].element?.text ?? ""
                    var latitude = customerXml["LatitudeValue"].element?.text ?? ""
                    var longitude = customerXml["LongitudeValue"].element?.text ?? ""
                    let route = customerXml["Route"].element?.text ?? ""
                    let channel = customerXml["Channel"].element?.text ?? ""
                    let cardGroup = customerXml["CardGroup"].element?.text ?? ""
                    let lastVisited = customerXml["LastVisited"].element?.text ?? ""
                    
                    longitude = longitude.trimmingCharacters(in: .whitespaces)
                    latitude = latitude.trimmingCharacters(in: .whitespaces)
                    
                    let customer = Customer()
                    customer.CardCode = code
                    customer.CardName = name
                    customer.Address = address
                    customer.ContactPerson = contact
                    customer.Tel = tel
                    customer.LatitudeValue = latitude
                    customer.LongitudeValue = longitude
                    customer.Route = route
                    customer.Channel = channel
                    customer.CustGrp = cardGroup
                    customer.lastVisit = lastVisited
            
                    results.append(customer)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func searchCustomers(UserCode: String, Route: String, Channel: String, Group: String, SalesManId: String, FreeText: String, WorkingDate: String, onSucess: @escaping ([Customer]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + search_customer_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "Route" : Route, "Channel": Channel, "Group" : Group, "SalesManId" : SalesManId, "FreeText" : FreeText, "WorkingDate" : WorkingDate]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Customer] = []
                
                if count > 0 {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let code = customerXml["CardCode"].element?.text ?? ""
                        let name = customerXml["CardName"].element?.text ?? ""
                        let address = customerXml["Address"].element?.text ?? ""
                        let contact = customerXml["ContactPerson"].element?.text ?? ""
                        let tel = customerXml["Tel"].element?.text ?? ""
                        var latitude = customerXml["LatitudeValue"].element?.text ?? ""
                        var longitude = customerXml["LongitudeValue"].element?.text ?? ""
                        let route = customerXml["Route"].element?.text ?? ""
                        let channel = customerXml["Channel"].element?.text ?? ""
                        let cardGroup = customerXml["CardGroup"].element?.text ?? ""
                        let lastVisited = customerXml["LastVisited"].element?.text ?? ""
                        
                        longitude = longitude.trimmingCharacters(in: .whitespaces)
                        latitude = latitude.trimmingCharacters(in: .whitespaces)
                        
                        let customer = Customer()
                        customer.CardCode = code
                        customer.CardName = name
                        customer.Address = address
                        customer.ContactPerson = contact
                        customer.Tel = tel
                        customer.LatitudeValue = latitude
                        customer.LongitudeValue = longitude
                        customer.Route = route
                        customer.Channel = channel
                        customer.CustGrp = cardGroup
                        customer.lastVisit = lastVisited
                        
                        results.append(customer)
                    }
                    
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getNoteFromUser(UserCode: String, CardCode: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + get_note_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "CardCode" : CardCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result = table[0]["NoteString"].element?.text ?? ""
                
                onSucess(result)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getNoteType(UserCode: String, onSucess: @escaping ([NoteGroup]) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + get_note_type_url
        
        let pagram: Parameters = ["UserCode" : UserCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[NoteGroup] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["NotesGroup"].element?.text ?? ""
                    let name = customerXml["NotesGroupName"].element?.text ?? ""
                    
                    let noteType = NoteGroup()
                    noteType.NotesGroup = code
                    noteType.NotesGroupName = name
                    
                    results.append(noteType)
                }
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    
    static func checkin(UserCode: String, CustCode: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + checkin_url
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let pagram: Parameters = ["UserCode" : UserCode, "CustCode": CustCode, "DeviceID": deviceID]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultID = customerXml["ResultID"].element?.text ?? ""
                    onSucess(ResultID)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func addNote(UserCode: String, CustCode: String, CheckInID: String, NotesGroup: String, NotesRemark: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + add_note_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "CustCode": CustCode, "CheckInID": CheckInID, "NotesGroup": NotesGroup, "NotesRemark": NotesRemark]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultID = customerXml["ResultID"].element?.text ?? ""
                    onSucess(ResultID)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func checkinInStockAdd(UserCode: String, CustCode: String, CheckInID: String, ItemCode: String, BarCode: String, CaseInStock: Int, BottleInStock: Int, Remarks: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + stock_add_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "CustCode": CustCode, "CheckInID": CheckInID, "ItemCode": ItemCode, "CaseInStock": CaseInStock, "Remarks": Remarks, "BottleInStock": BottleInStock, "BarCode" : BarCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultID = customerXml["ResultID"].element?.text ?? ""
                    onSucess(ResultID)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func addCustomer(CardName: String, ContactPerson: String, Tel: String, LatitudeValue: String, LongitudeValue: String, CardGroup: String, Reamarks:
        String, Address: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + add_customer_url
        
        let pagram: Parameters = ["CardName" : CardName, "ContactPerson": ContactPerson, "Tel": Tel, "LatitudeValue": LatitudeValue, "LongitudeValue": LongitudeValue, "CardGroup": CardGroup, "Reamarks": Reamarks, "Address" : Address]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultString = customerXml["ResultString"].element?.text ?? ""
                    onSucess(ResultString)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func checkOut(UserCode: String, CustCode: String, CheckInID: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + checkout_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "CustCode": CustCode, "CheckInID": CheckInID]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                
                print(xml)
                
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                print(table)
                
                let count = table.all.count
                
                if count > 0 {
                    let customerXml = table[0]
                    let ResultID = customerXml["ResultID"].element?.text ?? ""
                    onSucess(ResultID)
                } else {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
                }
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getStatisticFromUser(UserCode: String, CardCode: String, onSucess: @escaping ([StatisticModel]) -> (), onError: @escaping (String) -> ()) {
        let url = getCurrentApiUrl() + get_statistic_url
        
        let pagram: Parameters = ["UserCode" : UserCode, "CustCode" : CardCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[StatisticModel] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        
                        let CustCode = customerXml["NotesGroup"].element?.text ?? ""
                        let CheckInTime = customerXml["CheckInTime"].element?.text ?? ""
                        let CheckOutTime = customerXml["CheckOutTime"].element?.text ?? ""
                        let NoCustOrder = customerXml["NoCustOrder"].element?.text ?? ""
                        let NoPicTake = customerXml["NoPicTake"].element?.text ?? ""
                        
                        let statistic = StatisticModel(CustCode: CustCode, CheckInTime: CheckInTime, CheckOutTime: CheckOutTime, NoCustOrder: NoCustOrder, NoPicTake: NoPicTake)
                        
                        results.append(statistic)
                    }
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
}
