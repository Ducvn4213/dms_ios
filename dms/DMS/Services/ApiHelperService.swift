//
//  CustomerServices.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash
import MagicalRecord

class ApiHelperService {
    static func getChannels(userCode: String, onSucess: @escaping ([Channel]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_channels_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Channel] = []
                if (count == 0) {
                    onSucess(results)
                    return
                }
                
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["ChannelCode"].element?.text ?? ""
                    let name = customerXml["ChannelName"].element?.text ?? ""
                    
                    let channel = Channel()
                    channel.ChannelCode = code
                    channel.ChannelName = name
                    
                    results.append(channel)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getCustomerGroups(userCode: String, onSucess: @escaping ([CustomerGroup]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_customer_group_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[CustomerGroup] = []
                
                if (count == 0) {
                    onSucess(results)
                }
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["GrpCode"].element?.text ?? ""
                    let name = customerXml["GrpName"].element?.text ?? ""
                    
                    let customerGroup = CustomerGroup()
                    customerGroup.GrpCode = code
                    customerGroup.GrpName = name
                    
                    results.append(customerGroup)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }

    static func getOrders(UserCode: String, FromDate: String = "2016/01/01", ToDate: String, DocStatus: String = "01", VisitStatus: String = "01", OrderType: String = "01", Route: String = "01", FreeText: String = "", onSucess: @escaping ([Order]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_orders_url
        
        let pagram: Parameters = [
            "UserCode" : UserCode,
            "FromDate" : FromDate,
            "ToDate" : ToDate,
            "DocStatus" : DocStatus,
            "VisitStatus" : VisitStatus,
            "OrderType" : OrderType,
            "Route" : Route,
            "FreeText" : FreeText]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Order] = []
                
                if(count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let docEntry = customerXml["DocEntry"].element?.text ?? ""
                        let cardCode = customerXml["CardCode"].element?.text ?? ""
                        let cardName = customerXml["CardName"].element?.text ?? ""
                        let address = customerXml["Address"].element?.text ?? ""
                        let route = customerXml["Route"].element?.text ?? ""
                        let orderType = customerXml["OrderType"].element?.text ?? ""
                        let orderTypeName = customerXml["OrderTypeName"].element?.text ?? ""
                        let poNo = customerXml["PONo"].element?.text ?? ""
                        let poDate = customerXml["PODate"].element?.text ?? ""
                        let deliveryNo = customerXml["DeliveryNo"].element?.text ?? ""
                        let deliveryDate = customerXml["DeliveryDate"].element?.text ?? ""
                        let docStatus = customerXml["DocStatus"].element?.text ?? ""
                        let docStatusName = customerXml["DocStatusName"].element?.text ?? ""
                        let salesEmp = customerXml["SalesEmp"].element?.text ?? ""
                        let deliveryEmp = customerXml["DeliveryEmp"].element?.text ?? ""
                        let docTotal = customerXml["DocTotal"].element?.text ?? ""
                        let docVATTotal = customerXml["DocVATTotal"].element?.text ?? ""
                        let docGToal = customerXml["DocGToal"].element?.text ?? ""
                        let remark = customerXml["Remark"].element?.text ?? ""
                        let createDate = customerXml["CreateDate"].element?.text ?? ""
                        let noteDelivery = customerXml["NotesDelivery"].element?.text ?? ""
                        let deliveryDate_Act = customerXml["DeliveryDate_Act"].element?.text ?? ""
                        
                        let order = Order()
                        order.DocEntry = docEntry
                        order.CardCode = cardCode
                        order.CardName = cardName
                        order.Address = address
                        order.Route = route
                        order.OrderType = orderType
                        order.OrderTypeName = orderTypeName
                        order.PONo = poNo
                        order.PODate = poDate
                        order.DeliveryNo = deliveryNo
                        order.DeliveryDate = deliveryDate
                        order.DocStatus = docStatus
                        order.DocStatusName = docStatusName
                        order.SalesEmp = salesEmp
                        order.DeliveryEmp = deliveryEmp
                        order.DocTotal = docTotal
                        order.DocVATTotal = docVATTotal
                        order.DocGToal = docGToal
                        order.Remark = remark
                        order.CreateDate = createDate
                        order.NotesDelivery = noteDelivery
                        order.DeliveryDate_Act = deliveryDate_Act
                        
                        if (order.isAbleToAdd()) {
                            results.append(order)
                        }
                    }
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getVisitTypes(userCode: String, onSucess: @escaping ([TypeVisit]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_type_visit_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[TypeVisit] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["StatusCode"].element?.text ?? ""
                    let name = customerXml["StatusName"].element?.text ?? ""
                    
                    let typeVisit = TypeVisit()
                    typeVisit.StatusCode = code
                    typeVisit.StatusName = name
                    
                    results.append(typeVisit)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getOrderTypes(userCode: String, onSucess: @escaping ([TypeOrder]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_type_order_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[TypeOrder] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["TypeCode"].element?.text ?? ""
                    let name = customerXml["TypeName"].element?.text ?? ""
                    
                    let type = TypeOrder()
                    type.TypeCode = code
                    type.TypeName = name
                    
                    results.append(type)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getStatusOrders(userCode: String, onSucess: @escaping ([StatusOrder]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_status_order_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[StatusOrder] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["StatusCode"].element?.text ?? ""
                    let name = customerXml["StatusName"].element?.text ?? ""
                    
                    let status = StatusOrder()
                    status.StatusCode = code
                    status.StatusName = name
                    
                    results.append(status)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getWareHouseList(userCode: String, onSucess: @escaping ([WareHouse]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_warehouse_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[WareHouse] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["WhsCode"].element?.text ?? ""
                    let name = customerXml["WhsName"].element?.text ?? ""
                    
                    let ware = WareHouse()
                    ware.WhsCode = code
                    ware.WhsName = name
                    
                    results.append(ware)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getRouteList(userCode: String, onSucess: @escaping ([Route]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_route_url
        
        let pagram: Parameters = ["UserCode" : userCode]
        
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Route] = []
                
                for i in 0...(count - 1) {
                    let customerXml = table[i]
                    let code = customerXml["RouteCode"].element?.text ?? ""
                    let name = customerXml["RouteName"].element?.text ?? ""
                    
                    let route = Route()
                    route.RouteCode = code
                    route.RouteName = name
                    
                    results.append(route)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getLeaveReason(onSuccess: @escaping ([LeaveReason]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_leave_reason
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSuccess([])
                    return
                }
                
                var results:[LeaveReason] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let code = item["LCode"].element?.text ?? ""
                    let name = item["LName"].element?.text ?? ""
                    
                    let reason = LeaveReason()
                    reason.Code = code
                    reason.Name = name
                    
                    results.append(reason)
                }
                
                onSuccess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
    
    static func getLeaveType(onSuccess: @escaping ([LeaveType]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_leave_type
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSuccess([])
                    return
                }
                
                var results:[LeaveType] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let code = item["LCode"].element?.text ?? ""
                    let name = item["LName"].element?.text ?? ""
                    
                    let type = LeaveType()
                    type.Code = code
                    type.Name = name
                    
                    results.append(type)
                }
                
                onSuccess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
    
    static func getLeaveLog(onSuccess: @escaping ([LeaveLog]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_leave_log
        
        let pagram: Parameters = ["UserCode" : APPDELEGATE.userCode]
        
        Alamofire.request(url, method: .get, parameters: pagram, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSuccess([])
                    return
                }
                
                var results:[LeaveLog] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let id = item["LeaveID"].element?.text ?? ""
                    let usercode = item["UserCode"].element?.text ?? ""
                    let deviceid = item["DeviceID"].element?.text ?? ""
                    let from = item["FromDate"].element?.text ?? ""
                    let to = item["ToDate"].element?.text ?? ""
                    let type = item["LeaveType"].element?.text ?? ""
                    let reason = item["Reason"].element?.text ?? ""
                    let note = item["Remarks"].element?.text ?? ""
                    let status = item["Status"].element?.text ?? ""
                    let createdate = item["CreateDate"].element?.text ?? ""
                    let approvedate = item["ApproveDate"].element?.text ?? ""
                    let approveuser = item["ApproveUser"].element?.text ?? ""
                    
                    let log = LeaveLog(
                        id: id,
                        userCode: usercode,
                        deviceID: deviceid,
                        fromDate: from,
                        toDate: to,
                        type: type,
                        reason: reason,
                        remarks: note,
                        status: status,
                        createDate: createdate,
                        approveDate: approvedate,
                        approveUser: approveuser)
                    
                    results.append(log)
                }
                
                onSuccess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
    
    static func approveLeave(id: String, onSuccess: @escaping (String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + approve_leave
        
        let pagram: Parameters = [
            "USerCode" : APPDELEGATE.userCode,
            "LeaveID" : id
        ]
        
        Alamofire.request(url, method: .get, parameters: pagram, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["ResultID"].element?.text ?? ""
                
                onSuccess(result)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }

    static func addLeave(deviceID: String, type: String, from: String, to: String, reason: String, note: String, onSuccess: @escaping (String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + add_leave_ticket
        
        let pagram: Parameters = [
            "UserCode" : APPDELEGATE.userCode,
            "DeviceID" : deviceID,
            "FromDate" : from,
            "ToDate" : to,
            "LeaveType" : type,
            "Reason" : reason,
            "Reamarks" : note
        ]
        
        Alamofire.request(url, method: .get, parameters: pagram, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["ResultID"].element?.text ?? ""
                
                onSuccess(result)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
}

