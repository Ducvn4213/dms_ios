//
//  LoginServices
//  DMS
//
//  Created by KID on 10/10/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

class LoginServices {
    static func login(UserCode : String, Password : String, onSucess: @escaping (Bool, String, String, String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/IDN_SmartPhone_Login_DeviceID"
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        let parameters = ["UserCode":UserCode, "Password":Password, "DeviceID": deviceID]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["Result"].element?.text ?? ""
                let userCode : String =  table["UserCode"].element?.text ?? ""
                let SalesManId : String =  table["SalesManId"].element?.text ?? ""
                let WorkingDate : String =  table["WorkingDate"].element?.text ?? ""
                onSucess(result == "1", userCode, SalesManId, WorkingDate)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
    
    static func changePassword(userCode : String, oldPassword : String, newPassword: String, onSucess: @escaping (String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/iD_OUSR_ChangePassword"
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        
        let parameters = [
            "UserCode": userCode,
            "OldPass": oldPassword,
            "NewPassword": newPassword,
            "DeviceID": deviceID
        ]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["AlertSystem"].element?.text ?? ""
                
                if (result.contains("UnSuccessfully")) {
                    onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
                    return
                }
        
                onSucess(result)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
}
