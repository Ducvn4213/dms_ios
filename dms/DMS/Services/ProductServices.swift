//
//  ProductServices.swift
//  DMS
//
//  Created by Thanh Tanh on 7/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

class ProductServices {
    static func getProducts(FreeText: String = "", onSucess: @escaping ([Product]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + item_list_sales_search_url
        let pagram: Parameters = ["UserCode": APPDELEGATE.userCode, "FreeText": FreeText]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Product] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let barcode = customerXml["CodeBars"].element?.text ?? ""
                        let name = customerXml["ItemName"].element?.text ?? ""
                        let id = customerXml["ItemCode"].element?.text ?? ""
                        let BuyUoM = customerXml["BuyUoM"].element?.text ?? ""
                        let InvUom = customerXml["InvUom"].element?.text ?? ""
                        let FrgnName = customerXml["FrgnName"].element?.text ?? ""
                        
                        let product = Product()
                        product.ItemCode = id
                        product.CodeBars = barcode
                        product.ItemName = name
                        product.FrgnName = FrgnName
                        product.BuyUoM = BuyUoM
                        product.InvUoM = InvUom
                        product.even = 0
                        product.retail = 0
                        product.barcode = barcode
                        
                        results.append(product)
                    }
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getCustomerProducts(cusCode: String, onSucess: @escaping ([Product]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + item_list_checkin_instock_url
        let pagram: Parameters = ["UserCode": APPDELEGATE.userCode, "CustCode": cusCode]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Product] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let barcode = customerXml["CodeBars"].element?.text ?? ""
                        let name = customerXml["ItemName"].element?.text ?? ""
                        let id = customerXml["ItemCode"].element?.text ?? ""
                        let BuyUoM = customerXml["BuyUoM"].element?.text ?? ""
                        let InvUom = customerXml["InvUom"].element?.text ?? ""
                        let FrgnName = customerXml["FrgnName"].element?.text ?? ""
                        
//                        let product = ProductModel(CodeBars: barcode, ItemName: name, FrgnName: FrgnName, ItemCode: id, BuyUoM: BuyUoM, InvUom: InvUom, CaseInStock: 0, BottleInStock: 0)
                        let product = Product()
                        product.ItemCode = id
                        product.CodeBars = barcode
                        product.ItemName = name
                        product.FrgnName = FrgnName
                        product.BuyUoM = BuyUoM
                        product.InvUoM = InvUom
                        product.even = 0
                        product.retail = 0
                        product.barcode = barcode

                        results.append(product)
                    }
                }
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getProductPrice(CustCode: String = "", ItemCode: String = "", UoM: String = "", CurrDate: String = "", onSucess: @escaping (String, String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_item_price_url
        let pagram: Parameters = ["CustCode": CustCode, "ItemCode": ItemCode, "UoM": UoM, "CurrDate": CurrDate]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var price = ""
                var vat = ""
                
                if (count > 0) {
                    let customerXml = table[0]
                    price = customerXml["ItemPrice"].element?.text ?? ""
                    vat = customerXml["VAT"].element?.text ?? ""
                }
                
                onSucess(price, vat)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getPromoProducts(cusCode: String, UserCode: String, CurrDate: String, onSucess: @escaping ([Product]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_promotion_product_url
        let pagram: Parameters = ["UserCode": APPDELEGATE.userCode, "CustCode": cusCode, "CurrDate": CurrDate]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Product] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let barcode = customerXml["CodeBars"].element?.text ?? ""
                        let name = customerXml["ItemName"].element?.text ?? ""
                        let id = customerXml["ItemCode"].element?.text ?? ""
                        let product = Product()
                        product.ItemCode = id
                        product.CodeBars = barcode
                        product.ItemName = name
                        results.append(product)
                    }
                }
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getProductByBarCode(BarCode: String, onSucess: @escaping ([Product]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + get_item_by_barcode
        let pagram: Parameters = ["BarCode": BarCode]
        Alamofire.request(url, method: .post, parameters: pagram, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                var results:[Product] = []
                
                if (count > 0) {
                    for i in 0...(count - 1) {
                        let customerXml = table[i]
                        let barcode = customerXml["Barcode"].element?.text ?? ""
                        let name = customerXml["ItemName"].element?.text ?? ""
                        let id = customerXml["ItemCode"].element?.text ?? ""
                        let BuyUoM = customerXml["UoM"].element?.text ?? ""
                        let InvUom = customerXml["InvUom"].element?.text ?? ""
                        let ExpDate = customerXml["ExpDate"].element?.text ?? ""
                        let product = Product()
                        product.ItemCode = id
                        product.CodeBars = barcode
                        product.ItemName = name
                        product.barcode = barcode
                        product.BuyUoM = BuyUoM
                        product.InvUoM = InvUom
                        product.ExpDate = ExpDate
                        results.append(product)
                    }
                }
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
}
