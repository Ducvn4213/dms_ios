//
//  MonitorServices.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import Alamofire
import SWXMLHash

class MonitorServices {
    static func getStaffs(updateDate: String, onSucess: @escaping ([StaffModel]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/IDN_DMS_Employee_Monitor_Load?UserCode=manager&MonitorDate=" + updateDate
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSucess([])
                    return
                }
                
                var results:[StaffModel] = []
                
                for i in 0...(count - 1) {
                    let staffXml = table[i]
                    let id = staffXml["MyId"].element?.text ?? ""
                    let empId = staffXml["EmpId"].element?.text ?? ""
                    let name = staffXml["EmpFullName"].element?.text ?? ""
                    let picLink = staffXml["PicLink"].element?.text ?? ""
                    let date = staffXml["MyDate"].element?.text ?? ""
                    let checkInPlan = staffXml["CheckInPlan"].element?.text ?? ""
                    let checkInFinish = staffXml["CheckInFinish"].element?.text ?? ""
                    let noOrder = staffXml["NoOrder"].element?.text ?? ""
                    let lastCustName = staffXml["LastCustName"].element?.text ?? ""
                    let lastCustAdd = staffXml["LastCustAdd"].element?.text ?? ""
                    let noofMetter = (staffXml["NoofMetter"].element?.text ?? "0") + "m"
                    
                    let staff = StaffModel(id: id, empId: empId, name: name, avatarUrl: picLink, date: date, checkInPlan: checkInPlan, checkInFinish: checkInFinish, noOrder: noOrder, lastCustomerName: lastCustName, lastCustomerAddress: lastCustAdd, noofMetter: noofMetter)
                    results.append(staff)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getAllVisitedCustomers(forStaffId staffID: String, updateDate: String, onSucess: @escaping ([VisitingModel]) -> (), onError: @escaping ((String) -> ())) {
        let url = String(format: getCurrentApiUrl() + "/IDN_DMS_CustVisited_Emp_Load?EmpId=%@&MonitorDate=%@", staffID, updateDate)
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSucess([])
                    return
                }
                
                var results:[VisitingModel] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let id = item["MyId"].element?.text ?? ""
                    let latitude = item["LatitudeValue"].element?.text ?? "0"
                    let longitude = item["LongitudeValue"].element?.text ?? "0"
                    let order = item["VisitOrder"].element?.text ?? "0"
                    let name = item["CardName"].element?.text ?? ""
                    let address = item["Address"].element?.text ?? ""
                    let avatarUrl = item["PicLink"].element?.text ?? ""
                    let empName = item["EmpFullName"].element?.text ?? ""
                    let checkInPlan = item["CheckInPlan"].element?.text ?? ""
                    let checkInFinish = item["CheckInFinish"].element?.text ?? ""
                    let noOrder = item["NoOrder"].element?.text ?? ""
                    
                    let visiting = VisitingModel(id: id, latitude: Double(latitude)!, longitude: Double(longitude)!, order: Int(order)!, name: name, address: address, avatarUrl: avatarUrl, empName: empName, checkInPlan: checkInPlan, checkInFinish: checkInFinish, noOrder: noOrder)
                    results.append(visiting)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getAllStaffsLocation(onSucess: @escaping ([StaffLocationModel]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/IDN_DMS_EmpCurrLocation_Load?USerCode=manager"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSucess([])
                    return
                }
                
                var results:[StaffLocationModel] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let id = item["MyId"].element?.text ?? ""
                    let name = item["EmpFullName"].element?.text ?? ""
                    let avatarUrl = item["PicLink"].element?.text ?? ""
                    let latitude = item["CurrLat"].element?.text ?? "0"
                    let longitude = item["CurrLong"].element?.text ?? "0"
                    let order = item["NoOrder"].element?.text ?? "0"
                    let checkinPlan = item["CheckInPlan"].element?.text ?? "0"
                    let checkinFinished = item["CheckInFinish"].element?.text ?? "0"
                    
                    let staff = StaffLocationModel(id: id, name: name, avatarUrl: avatarUrl, latitude: Double(latitude)!, longitude: Double(longitude)!, order: order, checkinPlan: checkinPlan, checkinFinished: checkinFinished)
                    results.append(staff)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func getTimeSheets(onSucess: @escaping ([TimeSheetModel]) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/iD_TimeSheet_Log?UserCode=" + APPDELEGATE.userCode
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.httpBody, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let count = table.all.count
                if count <= 0 {
                    onSucess([])
                    return
                }
                
                var results:[TimeSheetModel] = []
                
                for i in 0...(count - 1) {
                    let item = table[i]
                    let id = item["TSId"].element?.text ?? ""
                    let userCode = item["UserCode"].element?.text ?? ""
                    let deviceId = item["DeviceID"].element?.text ?? ""
                    let startTime = item["StartTime"].element?.text ?? "0"
                    let latStart = item["LatitudeValue_Start"].element?.text ?? "0"
                    let longStart = item["LongitudeValue_Start"].element?.text ?? "0"
                    let endTime = item["EndTime"].element?.text ?? "0"
                    let latEnd = item["LatitudeValue_End"].element?.text ?? "0"
                    let longEnd = item["LongitudeValue_end"].element?.text ?? "0"
                    
                    let timeSheet = TimeSheetModel(tsId: id, userCode: userCode, deviceId: deviceId, startTime: startTime, latStart: latStart, longStart: longStart, endTime: endTime, latEnd: latEnd, longEnd: longEnd)
                    
                    results.append(timeSheet)
                }
                
                onSucess(results)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định")
            }
        }
    }
    
    static func addTimeSheet(lat: String, lon: String, onSucess: @escaping (String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/iD_TimeSheet_Add"
        
        let deviceID = UIDevice.current.identifierForVendor!.uuidString
        let userCode = APPDELEGATE.userCode!
        
        let parameters = [
            "UserCode": userCode,
            "DeviceID": deviceID,
            "LatitudeValue_Start": lat,
            "LongitudeValue_Start": lon
        ]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["ResultID"].element?.text ?? ""
            
                onSucess(result)
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
    
    static func updateTimeSheet(id: String, lat: String, lon: String, onSucess: @escaping (String) -> (), onError: @escaping ((String) -> ())) {
        let url = getCurrentApiUrl() + "/iD_TimeSheet_Update"
        
        let parameters = [
            "TSId": id,
            "LatitudeValue_End": lat,
            "LongitudeValue_End": lon
        ]
        
        Alamofire.request(url, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil).responseData { (response) in
            if let data = response.result.value {
                let xml = SWXMLHash.parse(data)
                print(xml.children)
                let table = xml["DataTable"]["diffgr:diffgram"]["NewDataSet"]["Table"]
                
                let result : String = table["ResultID"].element?.text ?? ""
                
                if (result == "1") {
                    onSucess(result)
                    return
                }
                
                onError("")
                
            } else {
                onError(response.result.error?.localizedDescription ?? "Lỗi không xác định".localized())
            }
        }
    }
}
