import Alamofire
import RealmSwift

class SyncService {
    let realm = try! Realm()
    
    class var sharedInstance: SyncService {
        struct Static {
            static let instance = SyncService()
        }
        return Static.instance
    }
    
    func downloadData(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        try! self.realm.write {
            realm.deleteAll()
        }
        loadLeaveTypes(userName: userName, onSucess: onSucess, onError: onError)
    }
    
    private func loadLeaveTypes(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getLeaveType(onSuccess: {
            (types) in
            try! self.realm.write {
                REALM.delete(REALM.objects(LeaveType.self))
                for t in types {
                    self.realm.add(t)
                }
            }
            
            self.loadLeaveReasons(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadLeaveReasons(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getLeaveReason(onSuccess: {
            (reasons) in
            try! self.realm.write {
                REALM.delete(REALM.objects(LeaveReason.self))
                for r in reasons {
                    self.realm.add(r)
                }
            }
            
            self.loadListProduct(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListProduct(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ProductServices.getProducts(onSucess: {
            (products) in
            try! self.realm.write {
                REALM.delete(REALM.objects(Product.self))
                for p in products {
                    self.realm.add(p)
                }
            }
            
            self.loadListCustomer(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
                onError(error)
        }
    }
    
    private func loadListCustomer(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        CustomerServices.searchCustomers(UserCode: APPDELEGATE.userCode, Route: "00", Channel: "00", Group: "00", SalesManId: APPDELEGATE.SalesManId, FreeText: "", WorkingDate: APPDELEGATE.WorkingDate, onSucess: {
            (customers) in
            try! self.realm.write {
                REALM.delete(REALM.objects(Customer.self))
                for c in customers {
                    self.realm.add(c)
                }
            }
            
            self.loadListChannel(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListChannel(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getChannels(userCode: userName, onSucess: {
            (channels) in
            try! self.realm.write {
                REALM.delete(REALM.objects(Channel.self))
                for c in channels {
                    self.realm.add(c)
                }
            }
            
            self.loadListGroupCus(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListGroupCus(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getCustomerGroups(userCode: userName, onSucess: {
            (customerGroups) in
            try! self.realm.write {
                REALM.delete(REALM.objects(CustomerGroup.self))
                for cg in customerGroups {
                    self.realm.add(cg)
                }
            }
            
            self.loadListOrder(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListOrder(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getOrders(UserCode: userName, ToDate: Date().toStringYYYYMMDD(), onSucess: {
            (orders) in
            try! self.realm.write {
                REALM.delete(REALM.objects(Order.self))
                for o in orders {
                    self.realm.add(o)
                }
            }
            
            self.loadListTypeVisit(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListTypeVisit(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getVisitTypes(userCode: userName, onSucess: {
            (types) in
            try! self.realm.write {
                REALM.delete(REALM.objects(TypeVisit.self))
                for t in types {
                    self.realm.add(t)
                }
            }
            
            self.loadListTypeOrder(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListTypeOrder(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getOrderTypes(userCode: userName, onSucess: {
            (types) in
            try! self.realm.write {
                REALM.delete(REALM.objects(TypeOrder.self))
                for t in types {
                    self.realm.add(t)
                }
            }
            
            self.loadListStatusOrder(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListStatusOrder(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getStatusOrders(userCode: userName, onSucess: {
            (status) in
            try! self.realm.write {
                REALM.delete(REALM.objects(StatusOrder.self))
                for s in status {
                    self.realm.add(s)
                }
            }
            
            self.loadListWareHouse(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListWareHouse(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getWareHouseList(userCode: userName, onSucess: {
            (wares) in
            try! self.realm.write {
                REALM.delete(REALM.objects(WareHouse.self))
                for w in wares {
                    self.realm.add(w)
                }
            }
            
            self.loadListRoute(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }
    
    private func loadListRoute(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        ApiHelperService.getRouteList(userCode: userName, onSucess: {
            (routes) in
            try! self.realm.write {
                REALM.delete(REALM.objects(Route.self))
                for r in routes {
                    self.realm.add(r)
                }
            }
            
            self.loadListNotesGroup(userName: userName, onSucess: onSucess, onError: onError)
        }) {
            (error) in
            onError(error)
        }
    }

    private func loadListNotesGroup(userName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        CustomerServices.getNoteType(UserCode: userName, onSucess: {
            (types) in
            try! self.realm.write {
                REALM.delete(REALM.objects(NoteGroup.self))
                for t in types {
                    self.realm.add(t)
               }
            }
            onSucess("Download data complete".localized())
        }) {
            (error) in
            onError(error)
        }
    }
    
    func processSyncWith(syncObjects: [SyncObject], onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        
        var newSyns = syncObjects
        
        if newSyns.count == 0 {
            onSucess("Đồng bộ thành công".localized())
            return
        }
        
        let item = newSyns[0]
        
        switch(item.SyncType) {
        case sync_checkin_type?:
            checkin(item: item, onSucess: {(string) in
                try! REALM.write {
                    REALM.delete(item)
                }
                newSyns.remove(at: 0)
                self.processSyncWith(syncObjects: newSyns, onSucess: onSucess, onError: onError)
            }, onError: onError)
            break
        case sync_order_type?:
            order(item: item, onSucess: {(string) in
                try! REALM.write {
                    REALM.delete(item)
                }
                newSyns.remove(at: 0)
                self.processSyncWith(syncObjects: newSyns, onSucess: onSucess, onError: onError)
            }, onError: onError)
            break
        case .none:
            break
        case .some(_):
            break
        }
    }
    
    func checkinNote(listNote: List<Note>, checkinId: String, custCode: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        
//        let newList = listNote
        
        let newList = List<Note>(listNote)
        
        if newList.count == 0 {
            onSucess("Checkin note thành công".localized())
            return
        }
        
        let note = newList[0]
        
        CustomerServices.addNote(UserCode: APPDELEGATE.userCode, CustCode: custCode, CheckInID: checkinId, NotesGroup: note.NotesGroup!, NotesRemark: note.NotesRemark!, onSucess: {(noteId) in
            print(noteId)
            newList.remove(at: 0)
            self.checkinNote(listNote: newList, checkinId: checkinId, custCode: custCode, onSucess: onSucess, onError: onError)
        }, onError: onError)
    }
    
    func checkinAddProduct(listProduct: List<Product>, checkinId: String, custCode: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        
//        let newList = listProduct;
        
        let newList = List<Product>(listProduct)
        
        if newList.count == 0 {
            onSucess("Checkin product thành công".localized())
            return
        }
        
        let product = newList[0]
        
        CustomerServices.checkinInStockAdd(UserCode: APPDELEGATE.userCode, CustCode: custCode, CheckInID: checkinId, ItemCode: product.ItemCode!, BarCode: product.CodeBars!, CaseInStock: product.even, BottleInStock: product.retail, Remarks: "", onSucess: {(id) in
            print(id)
            newList.remove(at: 0)
            self.checkinAddProduct(listProduct: newList, checkinId: checkinId, custCode: custCode, onSucess: onSucess, onError: onError)
        }, onError: onError)
    }
    
    
    func checkin(item: SyncObject, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        CustomerServices.checkin(UserCode: APPDELEGATE.userCode, CustCode: (item.Checkin?.CustCode)!, onSucess: {(checkinId) in
            self.checkinNote(listNote: (item.Checkin?.ListNote)!, checkinId: checkinId, custCode: (item.Checkin?.CustCode)!, onSucess: {(String) in
                
                self.checkinAddProduct(listProduct: (item.Checkin?.ListProduct)!, checkinId: checkinId, custCode: (item.Checkin?.CustCode)!, onSucess: {(string) in
                    
                    CustomerServices.checkOut(UserCode: APPDELEGATE.userCode, CustCode: (item.Checkin?.CustCode)!, CheckInID: checkinId, onSucess: onSucess, onError: onError)
                    
                }, onError: onError)
            }, onError: onError)
        }, onError: onError)
    }
    
    func order(item: SyncObject, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
        addOrder(orders: (item.Order?.orderList)!, Cardcode: (item.Order?.Cardcode)!, OrderDate: (item.Order?.OrderDate)!, DeliveryDate: (item.Order?.DeliveryDate)!, CardName: (item.Order?.CardName)!, onSucess: onSucess, onError: onError)
    }
    
    func addOrder(orders : List<ItemOrder>, Cardcode: String, OrderDate: String, DeliveryDate: String, CardName: String, onSucess: @escaping (String) -> (), onError: @escaping (String) -> ()) {
    
        if orders.count == 0 {
            onSucess("Add order thành công".localized())
            return
        }
        
        let newList = List<ItemOrder>(orders)
        
        let order = newList[0]
        
        OrderServices.addOrder(UserCode: APPDELEGATE.userCode, Cardcode: Cardcode, OrderDate: OrderDate, DeliveryDate: DeliveryDate, CardName: CardName, ItemCode: (order.item?.ItemCode)!, ItemName: (order.item?.ItemName)!, ItemType: order.ItemType!, Quantity: order.Quantity!, Uom: (order.item?.BuyUoM)!, Price: order.Price!, VAT: order.VAT!, onSucess: {(id) in
            
            newList.remove(at: 0)
            self.addOrder(orders: newList, Cardcode: Cardcode, OrderDate: OrderDate, DeliveryDate: DeliveryDate, CardName: CardName, onSucess: onSucess, onError: onError)
        }, onError: onError)
    }
}

extension Results {
    func toArray<T>(ofType: T.Type) -> [T] {
        var array = [T]()
        for i in 0 ..< count {
            if let result = self[i] as? T {
                array.append(result)
            }
        }
        
        return array
    }
}
