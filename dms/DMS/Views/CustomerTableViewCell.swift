//
//  CustomerTableViewCell.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class CustomerTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bookButton: UIButton!
    @IBOutlet weak var checkinButton: UIButton!
    @IBOutlet weak var shipButton: UIButton!
    
    @IBOutlet weak var visitedView: UIView!
    @IBOutlet weak var topView: UIView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var telLabel: UILabel!
    @IBOutlet weak var visitedLabel: UILabel!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var contactPersonLabel: UILabel!
    
    weak var delegate: CustomerTableViewCellDelegate?
    
    private var customer: Customer?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        bookButton.makeDefaultCornerRadius()
        checkinButton.makeDefaultCornerRadius()
    }

    func setupWithCustomer(customer: Customer) {
        self.customer = customer
        
        self.addressLabel.text = customer.Address
        self.telLabel.text = customer.Tel
        
        self.contactPersonLabel.text = customer.ContactPerson
        self.nameLabel.text = customer.CardName
        
        if customer.lastVisit != nil {
            if let visitedDate = customer.lastVisit {
                self.visitedLabel.text = visitedDate
            } else {
                self.visitedLabel.text = ""
            }
            
            self.visitedView.isHidden = false
            self.topView.backgroundColor = UIColor(hexString: "47b04b")
            self.nameLabel.textColor = UIColor.white
            self.contactPersonLabel.textColor = UIColor.white
        } else {
            self.visitedLabel.text = ""
            self.visitedView.isHidden = true
            self.topView.backgroundColor = UIColor(hexString: "c7efc4")
            self.nameLabel.textColor = UIColor.darkText
            self.contactPersonLabel.textColor = UIColor.darkText
        }
    }

    @IBAction func book() {
        self.delegate?.book(customer: self.customer!)
    }
    
    @IBAction func checkin() {
        if let customer = self.customer {
            self.delegate?.checkinWithCustomer(customer: customer)
        }
    }
    @IBAction func actionShip(_ sender: Any) {
        self.delegate?.ship()
    }
    
    @IBAction func openMap(_ sender: Any) {
        self.delegate?.map(customer: self.customer!)
    }
    
    @IBAction func actionViewNote(_ sender: Any) {
        self.delegate?.note(customer: self.customer!)
    }

    @IBAction func actionStatistic(_ sender: Any) {
        self.delegate?.statistic(customer: self.customer!)
    }
    
}

protocol CustomerTableViewCellDelegate: class {
    func book(customer: Customer)
    func checkinWithCustomer(customer: Customer)
    func ship()
    func map(customer: Customer)
    func note(customer: Customer)
    func statistic(customer: Customer)
}
