//
//  OrderListViewCell.swift
//  DMS
//
//  Created by KID on 1/22/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

class OrderListViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelOrderDate: UILabel!
    @IBOutlet weak var labelOrderDeliveryDate: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    
    @IBOutlet weak var labelDeliveryDateAct: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelNote: UILabel!
    
    @IBOutlet weak var btnUpdate: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    var containerViewCotroller: OrderListViewController?
    var data: Order?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(data: Order) {
        self.data = data
        labelName.text = data.CardName! + " - " + data.PONo!
        labelType.text = String.init(format: "order_type_title".localized(), arguments: [data.OrderTypeName!])
        labelOrderDate.text = String.init(format: "order_date_title".localized(), arguments: [toDisplayDate(input: data.PODate!)])
        labelOrderDeliveryDate.text = String.init(format: "order_delivery_date_title".localized(), arguments: [toDisplayDate(input: data.DeliveryDate)])
        labelTotal.text = String.init(format: "order_total_cost_title".localized(), arguments: [data.DocTotal!])
        
        labelAddress.text = data.Address
        labelNote.text = String.init(format: "order_note_title".localized(), arguments: [data.NotesDelivery!])
        labelDeliveryDateAct.text = String.init(format: "order_delivery_date_act_title".localized(), arguments: [toDisplayDate(input: data.DeliveryDate_Act)])
        
        let docStatus = data.DocStatus
        if (docStatus == "02" || docStatus == "03" || docStatus == "04") {
            btnUpdate.isHidden = false
            btnCancel.isHidden = false
            
            if (docStatus == "02") {
                btnUpdate.isHidden = true
            }
        }
        else {
            btnUpdate.isHidden = true
            btnCancel.isHidden = true
        }
    }
    
    func toDisplayDate(input: String?) -> String {
        if (input == nil) {
            return "None";
        }
        
        var returnValue = input!.stringToDateTimeString(inputFormat: "yyyy-MM-dd'T'HH:mm:ssZ", outputFormat: "yyyy/MM/dd")
        
        if (returnValue.isEmpty) {
            returnValue = input!.stringToDateTimeString(inputFormat: "yyyy-MM-dd'T'HH:mm:ss.SSZ", outputFormat: "yyyy/MM/dd")
        }
        
        return returnValue
    }
    
    @IBAction func onUpdateClicked() {
        self.containerViewCotroller?.doUpdateOrder(order: (self.data!))
    }
    
    @IBAction func onCancelClicked() {
        let title = "dialog_title_notice".localized()
        let message = "order_cancel_message".localized()
        let ok = "dialog_ok".localized()
        let cancel = "dialog_cancel".localized()
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: ok, style: .default, handler: { action in
            self.containerViewCotroller?.doCancelOrder(docEntry: (self.data?.DocEntry!)!)
        }))
        alert.addAction(UIAlertAction(title: cancel, style: .default, handler: nil))
    
        
        self.containerViewCotroller?.present(alert, animated: true, completion: nil)
    }
}
