//
//  OrderViewCell.swift
//  DMS
//
//  Created by KID on 12/20/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

protocol OrderItemDelegate {
    func amountChange(_ amount: String, sender: UITextField)
}

class OrderViewCell: UITableViewCell {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCode: UILabel!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var labelType: UILabel!
    @IBOutlet weak var labelUnit: UILabel!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelVat: UILabel!
    
    var delegate: OrderItemDelegate!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func actionEditAmount(_ sender: UITextField) {
        delegate.amountChange(txtAmount.text!, sender: sender)
    }
    
}
