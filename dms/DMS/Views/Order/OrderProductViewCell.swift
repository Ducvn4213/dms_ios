//
//  OrderProductViewCell.swift
//  DMS
//
//  Created by KID on 1/4/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

class OrderProductViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelGia: UILabel!
    @IBOutlet weak var labelVAT: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
