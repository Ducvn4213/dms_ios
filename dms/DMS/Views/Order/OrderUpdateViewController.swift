import UIKit
import PopupDialog

class OrderUpdateViewController: BaseViewController {
    
    @IBOutlet weak var type: MyDropDownView!
    @IBOutlet weak var txtDeliveryDate: UITextField!
    @IBOutlet weak var edtNote: UITextView!
    
    weak var dialog: PopupDialog? = nil
    
    let datePickerView : UIDatePicker = UIDatePicker()
    var orderData : Order?
    var parentContainer : OrderListViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configUI()
        loadData()
    }
    
    private func configUI() {
        edtNote.layer.cornerRadius = 5
        edtNote.layer.borderWidth = 1
        edtNote.layer.borderColor = UIColor.gray.cgColor
        edtNote.layer.masksToBounds = true
    }
    
    private func loadData() {
        let orderStatuses = REALM.objects(StatusOrder.self)
        var statuses : [String] = []
        for obj in orderStatuses {
            statuses.append(obj.StatusName!)
        }
        
        statuses.remove(at: 0)
        type.dropDownView.dataSource = statuses
        type.setDefault(index: 0)
        
        txtDeliveryDate.text = getCurrentNormalTime()
    }
    
    @IBAction func onExitClicked(_ sender: Any) {
        dialog?.dismiss()
    }
    
    @IBAction func onUpdateClicked(_ sender: Any) {
        let statusIndex = type.dropDownView.indexForSelectedRow! + 1
        let orderStatuses = REALM.objects(StatusOrder.self)
        
        let status = orderStatuses[statusIndex].StatusCode
        let date = txtDeliveryDate.text
        let note = edtNote.text
        
        dialog?.dismiss()
        parentContainer?.doUpdateOrder(status: status!, date: date!, note: note!, order: orderData!)
    }
    
    @IBAction func onDateClicked(_ sender: UITextField) {
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if (sender.text! != "") {
            datePickerView.date = dateFormatter.date(from: sender.text!)!
        }
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        sender.inputAccessoryView = toolBar
    }
    
    func donePicker() {
        txtDeliveryDate.text = datePickerView.date.toStringYYYYMMDD()
        txtDeliveryDate.endEditing(true)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        txtDeliveryDate.text = sender.date.toStringYYYYMMDD()
    }
}
