//
//  MyDropDownView.swift
//  DMS
//
//  Created by Mac Mini on 11/6/17.
//  Copyright © 2017 thach.tran. All rights reserved.
//

import UIKit
import DropDown

@IBDesignable
class MyDropDownView: ViewBase {

    @IBOutlet weak var txtValue: UITextField!
    
    lazy var dropDownView:DropDown = DropDown()
    
    var valueString:String?{
        return self.txtValue.text
    }
    
    
    @IBInspectable var holderString:String = "" {
        didSet {
            self.txtValue.placeholder = self.holderString
        }
    }
    
    @IBInspectable var dataSourceString:String? {
        didSet {
            if let dataString = self.dataSourceString {
                let array = dataString.components(separatedBy: ",")
                dropDownView.dataSource = array
            }
        }
    }
    
    override func setupView() {
        
        self.dropDownView.anchorView = txtValue
        self.dropDownView.bottomOffset = CGPoint(x: 0, y: txtValue.bounds.height)
        self.dropDownView.selectionAction = { [unowned self] (index: Int, item: String) in
            self.txtValue.text = item
            print("Selected item: \(item) at index: \(index)")
        }
    }
    
    @IBAction func click_dropdown(_ sender: Any) {
        self.dropDownView.show()
    }
    
    func setDefault(index: Int = 0) {
        if dropDownView.dataSource.count - 1 >= index {
            dropDownView.selectRow(at: index)
            txtValue.text = dropDownView.dataSource[index]
        }
    }
    
    
}
