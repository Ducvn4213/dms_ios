//
//  OrderFilterViewController.swift
//  DMS
//
//  Created by KID on 12/13/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import PopupDialog

class OrderFilterViewController: BaseViewController {
    
    
    @IBOutlet weak var txtTo: UITextField!
    @IBOutlet weak var txtFrom: UITextField!
    @IBOutlet weak var from: UITextField!
    @IBOutlet weak var to: UITextField!
    @IBOutlet weak var router: MyDropDownView!
    @IBOutlet weak var type: MyDropDownView!
    @IBOutlet weak var status: MyDropDownView!
    @IBOutlet weak var statusCheckin: MyDropDownView!
    @IBOutlet weak var btnExit: UIButton!
    weak var dialog: PopupDialog? = nil
    
    var filter: OrderFilter!
    
    var search: ((_ router: Int, _ status: Int, _ type: Int, _ statusCheckin: Int, _ from: String, _ to: String) -> ())!
    
    let datePickerView:UIDatePicker = UIDatePicker()
    let datePickerViewTo:UIDatePicker = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func loadData() {
        txtTo.text = filter.toTime
        txtFrom.text = filter.fromTime
        
        var types = ["Loại đơn hàng".localized()]
        for obj in REALM.objects(TypeOrder.self) {
            types.append(obj.TypeName!)
        }
        
        type.dropDownView.dataSource = types
        type.setDefault(index: filter.type)
        
        var routers = ["Chọn tuyến đường"]
        
        for obj in REALM.objects(Route.self) {
            routers.append(obj.RouteName!)
        }
        
        router.dropDownView.dataSource = routers
        router.setDefault(index: filter.Route)
        
        var listStatus = ["Tình trạng đơn hàng".localized()]
        for obj in REALM.objects(StatusOrder.self) {
            listStatus.append(obj.StatusName!)
        }
        status.dropDownView.dataSource = listStatus
        status.setDefault(index: filter.status)
        
        var statusVisit = ["Tình trạng viếng thăm".localized()]
        for obj in REALM.objects(TypeVisit.self) {
            statusVisit.append(obj.StatusName!)
        }
        statusCheckin.dropDownView.dataSource = statusVisit
        statusCheckin.setDefault(index: filter.statusCheckin)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionExit(_ sender: Any) {
        dialog?.dismiss()
    }
    
    @IBAction func actionReset(_ sender: Any) {
        router.setDefault()
        statusCheckin.setDefault()
        type.setDefault()
        status.setDefault()
        to.text = Date().toStringYYYYMMDD()
        from.text = "2017/01/01"
    }
    
    @IBAction func actionSearch(_ sender: Any) {
        search(router.dropDownView.indexForSelectedRow!, status.dropDownView.indexForSelectedRow!, type.dropDownView.indexForSelectedRow!, statusCheckin.dropDownView.indexForSelectedRow!, from.text!, to.text!)
    }
    
    @IBAction func actionDTime(_ sender: UITextField) {
        
        datePickerView.datePickerMode = UIDatePickerMode.date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if (sender.text! != "") {
            datePickerView.date = dateFormatter.date(from: sender.text!)!
        }
        
        sender.inputView = datePickerView
        
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePicker))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        sender.inputAccessoryView = toolBar
    }
    
    func donePicker() {
        txtFrom.text = datePickerView.date.toStringYYYYMMDD()
        txtFrom.endEditing(true)
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        txtFrom.text = sender.date.toStringYYYYMMDD()
    }
    
    @IBAction func actionDTimeTo(_ sender: UITextField) {
        
        datePickerViewTo.datePickerMode = UIDatePickerMode.date
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy/MM/dd"
        
        if (sender.text! != "") {
            datePickerViewTo.date = dateFormatter.date(from: sender.text!)!
        }
        
        sender.inputView = datePickerViewTo
        
        datePickerViewTo.addTarget(self, action: #selector(datePickerValueChangedTo), for: UIControlEvents.valueChanged)
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor.black
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "OK", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePickerTo))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        
        toolBar.setItems([spaceButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        
        sender.inputAccessoryView = toolBar
    }
    
    func donePickerTo() {
        txtTo.text = datePickerViewTo.date.toStringYYYYMMDD()
        txtTo.endEditing(true)
    }
    
    func datePickerValueChangedTo(sender:UIDatePicker) {
        txtTo.text = sender.date.toStringYYYYMMDD()
    }
}
