//
//  CapturedImageItemView.swift
//  DMS
//
//  Created by Thanh Tanh on 5/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class CapturedImageItemView: KViewBase {
    @IBOutlet weak var imageView: UIImageView!
    
    var deleteAction:(() -> ())?
    
    @IBAction func deleteImage() {
        self.deleteAction?()
    }
}
