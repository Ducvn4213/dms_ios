//
//  ProductTableViewCell.swift
//  DMS
//
//  Created by Thanh Tanh on 7/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {

    @IBOutlet weak var barcodeTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var blockNumberTextField: UITextField!
    @IBOutlet weak var individualNumberTextField: UITextField!
    var index: Int!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure() {
        let product = APPDELEGATE.stocks[index]
        self.barcodeTextField.text = product.ExpDate
        self.nameTextField.text = product.ItemName
        self.blockNumberTextField.text = "\(product.even)"
        self.individualNumberTextField.text = "\(product.retail)"
    }
    
    @IBAction func blockEditEnd(_ sender: Any) {
        print("edit end")
        if !(blockNumberTextField.text?.isEmpty)! {
            APPDELEGATE.stocks[index].even = Int(blockNumberTextField.text!)!
        }
    }
    
    @IBAction func individualEditEnd(_ sender: Any) {
        if !(individualNumberTextField.text?.isEmpty)! {
            APPDELEGATE.stocks[index].retail = Int(individualNumberTextField.text!)!
        }
    }
    
}
