//
//  NoteViewController.swift
//  DMS
//
//  Created by KID on 11/20/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class NoteViewController: BaseViewController {

    @IBOutlet weak var txtTitle: UILabel!
    @IBOutlet weak var txtMessage: UILabel!
    
    var name: String = "Thông báo"
    var message: String = "Có một thông báo"
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        txtTitle.text = name
        txtMessage.text = message
    }
    
    override func setText() {
        super.setText()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
