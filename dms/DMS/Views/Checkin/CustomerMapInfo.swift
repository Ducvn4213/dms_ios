//
//  CustomerMapInfo.swift
//  DMS
//
//  Created by KID on 11/10/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class CustomerMapInfo: UIViewController {
    
    var customer: Customer!

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelCode: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = customer.CardName
        labelCode.text = customer.CardCode
        labelAddress.text = customer.Address
        labelPhone.text = customer.Tel
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
