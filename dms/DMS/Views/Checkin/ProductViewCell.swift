//
//  ProductViewCell.swift
//  DMS
//
//  Created by KID on 11/26/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class ProductViewCell: UITableViewCell {
    
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var labelGiaChan: UILabel!
    @IBOutlet weak var labelTonChan: UILabel!
    @IBOutlet weak var labelGiaLe: UILabel!
    @IBOutlet weak var labelTonLe: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var textGiaChan: UILabel!
    @IBOutlet weak var textTonChan: UILabel!
    @IBOutlet weak var textGiaLe: UILabel!
    @IBOutlet weak var textTonLe: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
