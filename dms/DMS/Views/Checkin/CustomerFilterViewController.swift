//
//  CustomerFilterViewController.swift
//  DMS
//
//  Created by KID on 12/13/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import PopupDialog

class CustomerFilterViewController: BaseViewController {
    
    
    @IBOutlet weak var group: MyDropDownView!
    @IBOutlet weak var type: MyDropDownView!
    @IBOutlet weak var sort: MyDropDownView!
    @IBOutlet weak var router: MyDropDownView!
    @IBOutlet weak var btnExit: UIButton!
    weak var dialog: PopupDialog? = nil
    
    var filter: CustomerFilter!
    
    var search: ((_ router: Int, _ sort: Int, _ type: Int, _ group: Int) -> ())!

    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    func loadData() {
        var groups = ["Nhóm khách hàng"]        
        for obj in REALM.objects(CustomerGroup.self) {
            groups.append(obj.GrpName!)
        }
        
        group.dropDownView.dataSource = groups
        group.setDefault(index: filter.Group)
        
        var routers = ["Chọn tuyến đường"]
        
        for obj in REALM.objects(Route.self) {
            routers.append(obj.RouteName!)
        }
        
        router.dropDownView.dataSource = routers
        router.setDefault(index: filter.Route)
        
        var channels = ["Loại khách hàng"]
        
        for obj in REALM.objects(Channel.self) {
            channels.append(obj.ChannelName!)
        }
        
        sort.setDefault(index: filter.sort)
        
        type.dropDownView.dataSource = channels
        type.setDefault(index: filter.Channel)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionExit(_ sender: Any) {
        dialog?.dismiss()
    }
    
    @IBAction func actionReset(_ sender: Any) {
        router.setDefault()
        group.setDefault()
        type.setDefault()
        sort.setDefault()
    }
    
    @IBAction func actionSearch(_ sender: Any) {
        search(router.dropDownView.indexForSelectedRow!, sort.dropDownView.indexForSelectedRow!, type.dropDownView.indexForSelectedRow!, group.dropDownView.indexForSelectedRow!)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
