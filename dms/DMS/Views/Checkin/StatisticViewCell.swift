//
//  StatisticViewCell.swift
//  DMS
//
//  Created by KID on 1/17/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

class StatisticViewCell: UITableViewCell {

    @IBOutlet weak var labelNum: UILabel!
    @IBOutlet weak var labelCheckin: UILabel!
    @IBOutlet weak var labelCheckout: UILabel!
    @IBOutlet weak var labelOrder: UILabel!
    @IBOutlet weak var labelPic: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
