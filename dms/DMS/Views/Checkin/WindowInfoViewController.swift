//
//  WindowInfoViewController.swift
//  DMS
//
//  Created by KID on 10/18/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class WindowInfoViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelTotal: UILabel!
    @IBOutlet weak var labelAlready: UILabel!
    @IBOutlet weak var labelInvoice: UILabel!
    
    var staff : StaffLocationModel!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        imageView.sd_setImage(with: URL(string: staff.avatarUrl))
        labelName.text = staff.name
        labelPhone.text = "-"
        labelTotal.text = staff.checkinPlan
        labelAlready.text = staff.checkinFinished
        labelInvoice.text = staff.order        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
