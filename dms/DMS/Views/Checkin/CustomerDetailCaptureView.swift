//
//  CustomerDetailCaptureView.swift
//  DMS
//
//  Created by Thanh Tanh on 5/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit

class CustomerDetailCaptureView: KViewBase {
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var captureButton: UIButton!
    @IBOutlet weak var noImageLabel: UILabel!
    
    weak var controller: UIViewController?
    
    var pickedImages: [UIImage] = []
    
    @IBAction func captureImage() {
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        
        self.controller?.present(imagePicker, animated: true, completion: nil)
    }
    
    func addImage(image:UIImage) {
        self.pickedImages.append(image)
        
        self.buildImages()
    }
    
    func removeImage(at index:Int) {
        self.pickedImages.remove(at: index)
        self.buildImages()
    }
    
    func buildImages() {
        self.containerView.removeAllSubviews()
        if self.pickedImages.count == 0 {
            noImageLabel.isHidden = false
            return
        }
        
        self.noImageLabel.isHidden = true
        
        let width = (UIScreen.main.bounds.width - 40) / 3
        var segment:CGFloat = 10
        for (index, image) in self.pickedImages.enumerated() {
            let imageView = CapturedImageItemView(frame: CGRect(x: segment, y: 0, width: width, height: width))
            imageView.imageView.image = image
            imageView.deleteAction = {
                self.removeImage(at: index)
            }
            self.containerView.addSubview(imageView)
            segment += width + 10
        }
    }
    
    func savePhoto() {
        for image in self.pickedImages {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        }
    }
}

extension CustomerDetailCaptureView: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.addImage(image: pickedImage)
        }
        
        controller?.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        controller?.dismiss(animated: true, completion: nil)

    }
}
