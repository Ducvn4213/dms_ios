//
//  AllNoteViewController.swift
//  DMS
//
//  Created by KID on 11/23/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import PopupDialog

class AllNoteViewController: UIViewController {

    @IBOutlet weak var dropDown: MyDropDownView!
    @IBOutlet weak var txtNote: IQTextView!
    
    weak var dialog: PopupDialog? = nil
    var save: ((_ note: Note) -> ())!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        var noteTypes = [String]()
        
        for type in REALM.objects(NoteGroup.self) {
            noteTypes.append(type.NotesGroupName!)
        }
        
        dropDown.dropDownView.dataSource = noteTypes
        dropDown.setDefault()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnOK(_ sender: Any) {
        let note = Note()
        note.NotesGroup = REALM.objects(NoteGroup.self)[dropDown.dropDownView.indexForSelectedRow!].NotesGroup!
        note.NotesRemark = txtNote.text
        save(note)
        dialog?.dismiss()
    }
    
    @IBAction func btnDelete(_ sender: Any) {
        txtNote.text = ""
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
