//
//  StaffTableViewCell.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import SDWebImage

class StaffTableViewCell: UITableViewCell {
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var visitPlanLabel: UILabel!
    @IBOutlet weak var lastCustomerLabel: UILabel!
    @IBOutlet weak var noofMetterLabel: UILabel!
    @IBOutlet weak var planLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var finshLabel: UILabel!
    
    var locationStaff : StaffLocationModel!
    var delegate : MapCellDelegate!
    
    
    func configure(withStaff staff:StaffModel) {
        avatarImageView.sd_setImage(with: URL(string: staff.avatarUrl))
        nameLabel.text = staff.name
        visitPlanLabel.attributedText = buildLastCustomerText(withStaff: staff)
        lastCustomerLabel.attributedText = buildVisitPlanText(withStaff: staff)
        noofMetterLabel.text = staff.noofMetter
    }
    
    private func buildVisitPlanText(withStaff staff:StaffModel) -> NSAttributedString {
        let greenText = NSAttributedString(string: "Tổng đơn hàng: ", attributes: [
            NSFontAttributeName: UIFont.systemFont(ofSize: 12),
            NSForegroundColorAttributeName: UIColor(hexString: "00b100")
            ])
        let result = NSMutableAttributedString(attributedString: greenText)
        return result
    }
    
    private func buildLastCustomerText(withStaff staff:StaffModel) -> NSAttributedString {
        let greenText = NSAttributedString(string: "VIẾNG THĂM: ", attributes: [
            NSFontAttributeName: UIFont.systemFont(ofSize: 12),
            NSForegroundColorAttributeName : UIColor(hexString: "00b100")
            ])
        let result = NSMutableAttributedString(attributedString: greenText)
        
        var lastCustomer = "Không có"
        if staff.lastCustomerName != "" {
            lastCustomer = String(format: "%@ - %@", staff.lastCustomerName, staff.lastCustomerAddress)
        }
        
        result.append(NSAttributedString(string: lastCustomer, attributes: [
            NSFontAttributeName: UIFont.systemFont(ofSize: 12)
            ]))
        return result
    }
    
    @IBAction func actionMap(_ sender: Any) {
        delegate.showMap(staff: locationStaff)
    }
    
}

protocol MapCellDelegate {
    func showMap(staff : StaffLocationModel)
}
