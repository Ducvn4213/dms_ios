//
//  AppDelegate.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import UIKit
import MagicalRecord
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var staffs: [StaffModel] = []
    var sLocations: [StaffLocationModel] = []
    
    var userCode: String!
    
    var stocks: [Product] = []
    
    var originStocks: [Product] = []
    
    var promoProducts: [Product] = []
    
    var WorkingDate = ""
    
    var SalesManId = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        MagicalRecord.setupCoreDataStack(withStoreNamed: "Model")
        IQKeyboardManager.sharedManager().enable = true
        
//        GMSServices.provideAPIKey("AIzaSyD-uiGvHucrkn_cGT4ECQQ3fyGTsisEu4A")
//        GMSPlacesClient.provideAPIKey("AIzaSyD-uiGvHucrkn_cGT4ECQQ3fyGTsisEu4A")
        
        GMSServices.provideAPIKey("AIzaSyA97UbcXwazwwoAQDLHO_MEdiibXA8z5Wo")
        GMSPlacesClient.provideAPIKey("AIzaSyA97UbcXwazwwoAQDLHO_MEdiibXA8z5Wo")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

