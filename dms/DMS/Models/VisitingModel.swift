//
//  VisitingModel.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct VisitingModel {
    let id: String
    let latitude: Double
    let longitude: Double
    let order: Int
    let name: String
    let address: String
    let avatarUrl: String
    let empName: String
    let checkInPlan: String
    let checkInFinish: String
    let noOrder: String
}
