struct LeaveLog {
    let id: String?
    let userCode: String?
    let deviceID: String?
    let fromDate: String?
    let toDate: String?
    let type: String?
    let reason: String?
    let remarks: String?
    let status: String?
    let createDate: String?
    let approveDate: String?
    let approveUser: String?
}
