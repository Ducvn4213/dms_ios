import RealmSwift
import Alamofire

class Product : Object {
    @objc dynamic var ItemCode: String? = nil
    @objc dynamic var CodeBars: String? = nil
    @objc dynamic var ItemName: String? = nil
    @objc dynamic var FrgnName: String? = nil
    @objc dynamic var BuyUoM: String? = nil
    @objc dynamic var InvUoM: String? = nil
    @objc dynamic var even = 0
    @objc dynamic var retail = 0
    @objc dynamic var barcode: String? = nil
    @objc dynamic var ExpDate: String? = nil
    
    func equals(product: Product) -> Bool {
        if self.CodeBars != product.CodeBars {
            return false
        }
        if self.ItemCode != product.ItemCode {
            return false
        }
        if self.ItemName != product.ItemName {
            return false
        }
        if self.even != product.even {
            return false
        }
        if self.retail != product.retail {
            return false
        }
        return true
    }
}

class ProductPrice : Object {
    @objc dynamic var product: Product? = nil
    @objc dynamic var price: String? = nil
    @objc dynamic var vat: String? = nil
}

class Customer: Object {
    @objc dynamic var CardCode: String? = nil
    @objc dynamic var CardName: String? = nil
    @objc dynamic var Address: String? = nil
    @objc dynamic var ContactPerson: String? = nil
    @objc dynamic var Tel: String? = nil
    @objc dynamic var LatitudeValue: String? = nil
    @objc dynamic var LongitudeValue: String? = nil
    @objc dynamic var LatLongString: String? = nil
    @objc dynamic var Route: String? = nil
    @objc dynamic var Channel: String? = nil
    @objc dynamic var CustGrp: String? = nil
    @objc dynamic var lastVisit: String? = nil
    @objc dynamic var SalesManID = 0
    @objc dynamic var VisitStartDate: String? = nil
    @objc dynamic var VisitEndDate: String? = nil
    @objc dynamic var VisitTotal: String? = nil
    @objc dynamic var CardName_Eng: String? = nil
    @objc dynamic var VisitStatus: String? = nil
    @objc dynamic var Address_Eng: String? = nil
    @objc dynamic var ContactPerson_Eng: String? = nil
}

class Channel: Object {
    @objc dynamic var ChannelCode: String? = nil
    @objc dynamic var ChannelName: String? = nil
}

class CustomerGroup: Object {
    @objc dynamic var GrpCode: String? = nil
    @objc dynamic var GrpName: String? = nil
}

class Order: Object {
    @objc dynamic var DocEntry: String? = nil
    @objc dynamic var CardCode: String? = nil
    @objc dynamic var CardName: String? = nil
    @objc dynamic var Address: String? = nil
    @objc dynamic var Route: String? = nil
    @objc dynamic var OrderType: String? = nil
    @objc dynamic var OrderTypeName: String? = nil
    @objc dynamic var PONo: String? = nil
    @objc dynamic var PODate: String? = nil
    @objc dynamic var DeliveryNo: String? = nil
    @objc dynamic var DeliveryDate: String? = nil
    @objc dynamic var DocStatus: String? = nil
    @objc dynamic var DocStatusName: String? = nil
    @objc dynamic var SalesEmp: String? = nil
    @objc dynamic var DeliveryEmp: String? = nil
    @objc dynamic var DocTotal: String? = nil
    @objc dynamic var DocVATTotal: String? = nil
    @objc dynamic var DocGToal: String? = nil
    @objc dynamic var Remark: String? = nil
    @objc dynamic var CreateDate: String? = nil
    @objc dynamic var NotesDelivery: String? = nil
    @objc dynamic var DeliveryDate_Act: String? = nil
    
    func isAbleToAdd() -> Bool {
        if ((DocEntry?.isEmpty)! || (CardCode?.isEmpty)! || (CardName?.isEmpty)! || (DocStatus?.isEmpty)!) {
            return false
        }
        
        return true
    }
}

class ItemOrder: Object {
    @objc dynamic var item: Product? = nil
    @objc dynamic var ItemType: String? = nil
    @objc dynamic var Quantity: String? = nil
    @objc dynamic var Price: String? = nil
    @objc dynamic var VAT: String? = nil
}

class AddOrder: Object {
    var orderList = List<ItemOrder>()
    @objc dynamic var Cardcode: String? = nil
    @objc dynamic var OrderDate: String? = nil
    @objc dynamic var DeliveryDate: String? = nil
    @objc dynamic var CardName: String? = nil
}

enum SynObjectType: String {
    case order
    case checkin
    case note
    case checkinOrder
}

class SyncObjects: Object {
    @objc dynamic var id: String? = NSUUID().uuidString
    dynamic var type = SynObjectType.checkin.rawValue
    var typeEnum: SynObjectType {
        get {
            return SynObjectType(rawValue: type)!
        }
        set {
            type = newValue.rawValue
        }
    }
//    @objc dynamic var data: Parameters? = nil
    @objc dynamic var parentId: String? = nil
    
    override static func primaryKey() -> String? {
        return "id"
    }
}

class TypeVisit: Object {
    @objc dynamic var StatusCode: String? = nil
    @objc dynamic var StatusName: String? = nil
}

class TypeOrder: Object {
    @objc dynamic var TypeCode: String? = nil
    @objc dynamic var TypeName: String? = nil
}

class StatusOrder: Object {
    @objc dynamic var StatusCode: String? = nil
    @objc dynamic var StatusName: String? = nil
}

class WareHouse: Object {
    @objc dynamic var WhsCode: String? = nil
    @objc dynamic var WhsName: String? = nil
}

class Route: Object {
    @objc dynamic var RouteCode: String? = nil
    @objc dynamic var RouteName: String? = nil
}

class NoteGroup: Object {
    @objc dynamic var NotesGroup: String? = nil
    @objc dynamic var NotesGroupName: String? = nil
}

class Note: Object {
    @objc dynamic var NotesGroup: String? = nil
    @objc dynamic var NotesRemark: String? = nil
}

class Checkin: Object {
    @objc dynamic var UserCode: String? = nil
    @objc dynamic var CustCode: String? = nil
    @objc dynamic var DeviceID: String? = nil
    var ListNote: List<Note> = List<Note>()
    var ListProduct: List<Product> = List<Product>()
}

class SyncObject: Object {
    @objc dynamic var SyncType: String? = nil
    @objc dynamic var Checkin: Checkin? = nil
    @objc dynamic var Order: AddOrder? = nil
}

class LeaveType: Object {
    @objc dynamic var Code: String? = nil
    @objc dynamic var Name: String? = nil
}

class LeaveReason: Object {
    @objc dynamic var Code: String? = nil
    @objc dynamic var Name: String? = nil
}
