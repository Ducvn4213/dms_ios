//
//  VisitedCustomer+CoreDataProperties.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
import CoreData


extension VisitedCustomer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VisitedCustomer> {
        return NSFetchRequest<VisitedCustomer>(entityName: "VisitedCustomer");
    }

    @NSManaged public var code: String?
    @NSManaged public var visitedDate: Date?

}
