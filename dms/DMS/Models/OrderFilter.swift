//
//  OrderFilter.swift
//  DMS
//
//  Created by KID on 1/22/18.
//  Copyright © 2018 dms. All rights reserved.
//

import UIKit

struct OrderFilter {
    let Route: Int
    let status: Int
    let statusCheckin: Int
    let FreeText: String
    let fromTime: String
    let toTime: String
    let type: Int
}


