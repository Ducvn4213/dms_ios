//
//  CustomerModel.swift
//  DMS
//
//  Created by Thanh Tanh on 5/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct CustomerModel {
    let code: String
    let name: String
    let address: String
    let contactPerson: String
    let tel: String
    let latitude: Double
    let longitude: Double
    
    var visited = false
    var visitedDate: Date?
}
