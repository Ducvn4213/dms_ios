//
//  Statistic.swift
//  DMS
//
//  Created by KID on 1/17/18.
//  Copyright © 2018 dms. All rights reserved.
//

import Foundation

struct StatisticModel {
    let CustCode: String
    let CheckInTime: String
    let CheckOutTime: String
    let NoCustOrder: String
    let NoPicTake: String
}
