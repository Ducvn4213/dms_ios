//
//  StaffModel.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct StaffModel {
    let id: String
    let empId: String
    let name: String
    let avatarUrl: String
    let date: String
    let checkInPlan: String
    let checkInFinish: String
    let noOrder: String
    let lastCustomerName: String
    let lastCustomerAddress: String
    let noofMetter: String
}
