//
//  StaffLocationModel.swift
//  DMS
//
//  Created by Thanh Tanh on 8/5/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct StaffLocationModel {
    let id: String
    let name: String
    let avatarUrl: String
    let latitude: Double
    let longitude: Double
    let order: String
    let checkinPlan: String
    let checkinFinished: String
}
