struct TimeSheetModel {
    let tsId: String
    let userCode: String
    let deviceId: String
    let startTime: String
    let latStart: String
    let longStart: String
    let endTime: String
    let latEnd: String
    let longEnd: String
}
