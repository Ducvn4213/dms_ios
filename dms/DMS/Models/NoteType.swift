//
//  NoteType.swift
//  DMS
//
//  Created by KID on 11/23/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct NoteType {
    let NotesGroup: String
    let NotesGroupName: String
}
