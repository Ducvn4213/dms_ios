//
//  ProductModel.swift
//  DMS
//
//  Created by Thanh Tanh on 7/31/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct ProductModel {
    let CodeBars: String
    let ItemName: String
    let FrgnName: String
    let ItemCode: String
    let BuyUoM: String
    let InvUom: String
    var CaseInStock: Int
    var BottleInStock: Int
    
    
    func equals(product: ProductModel) -> Bool {
        if self.CodeBars != product.CodeBars {
            return false
        }
        if self.ItemCode != product.ItemCode {
            return false
        }
        if self.ItemName != product.ItemName {
            return false
        }
        if self.CaseInStock != product.CaseInStock {
            return false
        }
        if self.BottleInStock != product.BottleInStock {
            return false
        }
        return true
    }
}
