//
//  File.swift
//  DMS
//
//  Created by KID on 1/24/18.
//  Copyright © 2018 dms. All rights reserved.
//

import Foundation

struct OrderItem {
    let MyId: String
    let DocEntry: String
    let ItemCode: String
    let BarCode: String
    let ItemName: String
    var Quantity: String
    let UoM: String
    let Price: String
    let Discount: String
    let PriceAfterDiscount: String
    let VATAmt: String
    let VATPercent: String
    let GTotal: String
    let DocEntry1: String
    let CardCode: String
    let CardName: String
    let Address: String
    let Route: String
    let OrderType: String
    let OrderTypeName: String
    let PONo: String
    let PODate: String
    let DeliveryNo: String
    let DeliveryDate: String
    let DocStatus: String
    let DocStatusName: String
    let SalesEmp: String
    let DeliveryEmp: String
    let DocTotal: String
    let DocVATTotal: String
    let DocGToal: String
    let Remark: String
    let CreateDate: String
}
