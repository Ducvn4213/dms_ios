//
//  NoteModel.swift
//  DMS
//
//  Created by KID on 11/30/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation

struct NoteModel {
    let NotesGroup: String
    let NotesRemark: String
}
