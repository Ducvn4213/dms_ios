//
//  CustomerFilter.swift
//  DMS
//
//  Created by KID on 12/18/17.
//  Copyright © 2017 dms. All rights reserved.
//

import Foundation
struct CustomerFilter {
    let Route: Int
    let Channel: Int
    let Group: Int
    let FreeText: String
    let sort: Int
}
